
[![pipeline status](https://gitlab.com/aditya08/logistic-regression-mpi/badges/master/pipeline.svg)](https://gitlab.com/aditya08/logistic-regression-mpi/-/commits/master)


A C++ and MPI version of Stochastic Gradient Descent (SGD) to solve the Logistic Regression problem with -1, +1 labels.
Also includes some sparse linear algebra routines along with an implementation of the Communication-Avoiding SGD method.
