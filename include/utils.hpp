// Copyright 2020 Aditya Devarakonda. All rights reserved.
#ifndef INCLUDE_UTILS_HPP_
#define INCLUDE_UTILS_HPP_

#include <mkl.h>

constexpr unsigned int MEM_ALIGN = 64;

// define function aliases for platform-specific aligned alloc/dealloc.
#ifdef __INTEL_COMPILER
    #define ALIGNED_ALLOC(ALIGNMENT, SIZE) _mm_malloc(SIZE, ALIGNMENT)
    #define ALIGNED_FREE(...) _mm_free(__VA_ARGS__)
#elif __GNUC__
  #define ALIGNED_ALLOC(...) aligned_alloc(__VA_ARGS__)
  #define ALIGNED_FREE(...) free(__VA_ARGS__)
#elif _WIN64
  // alignment and size args flipped in windows.
  #define ALIGNED_ALLOC(ALIGNMENT, SIZE) _aligned_malloc(SIZE, ALIGNMENT)
  // free does not correctly dealloc aligned memory.
  #define ALIGNED_FREE(...) _aligned_free(__VA_ARGS__)
#endif  // function aliases

#endif  // INCLUDE_UTILS_HPP_
