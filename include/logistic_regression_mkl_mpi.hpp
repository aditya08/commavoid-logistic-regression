// Copyright 2019 Aditya Devarakonda. All rights reserved.
#ifndef INCLUDE_LOGISTIC_REGRESSION_MPI_HPP_
#define INCLUDE_LOGISTIC_REGRESSION_MPI_HPP_

#include <mpi.h>

#include <vector>
#include <string>

#ifdef TAU_PROFILING
#include "TAU.h"
#endif

template <typename T>
struct Options {
  T tol;  // convergence tolerance.
  T eta;  // learning rate.
  unsigned int nrows;
  unsigned int ncols;
  unsigned int nnz;
  unsigned int seed;  // rng seed.
  unsigned int max_iters;  // maximum number of iterations.
  unsigned int s;  // communication-avoiding parameter.
  unsigned int chkpt_freq;  // checkpointing frequency.
  unsigned int log_freq;  // logging or printing frequency
  unsigned int batch_size;  // batch size for SGD.
  // selects the optimizer: SGD or CA-SGD (case-insensitive).
  std::string optimizer;
  // the filepath where to store the log. Default: ./logistic_regression.log
  std::string logfile_path;
  // the filepath where to store the model. Default: ./logistic_regression.model
  std::string modelfile_path;
  // the filepath where to store benchmark/timing information.
  // Default: ./logistic_regression.timing
  std::string timerfile_path;
};

// obtain MPI_Datatype for templated data types.
template<typename T> inline MPI_Datatype get_MPI_Datatype(T* t)
{return MPI_BYTE; }  // default.
template<> inline MPI_Datatype get_MPI_Datatype(float* t)
{return MPI_FLOAT; }
template<> inline MPI_Datatype get_MPI_Datatype(double* t)
{return MPI_DOUBLE; }

// routines that require MPI_Comm.
template <typename T>
T training_loss(MPI_Comm, unsigned int, int*, int*, T*, T*, T*);
template <typename T>
T training_accuracy(MPI_Comm, unsigned int, int*, int*, T*, T*, T*);
template <typename T>
void logistic_regression(MPI_Comm, Options<T>, int*, int*, T*, T*, T*);
void logistic_regression_mkl(MPI_Comm, Options<float>, int*, int*, float*, float*, float*, int);
template <typename T>
void mini_batch_logistic_regression(MPI_Comm,
Options<T>, int*, int*, T*, T*, T*);
void mini_batch_logistic_regression_mkl(MPI_Comm comm, Options<float> opt,
int* rowptr, int* colptr, float* data, float* labels, float* weights, int nthreads);

template <typename T>
void ca_logistic_regression(MPI_Comm, Options<T>, int*, int* , T*, T*, T*);
void ca_logistic_regression_mkl(MPI_Comm, Options<float>, int*, int* , float*, float*, float*, int);


template<typename T>
void mini_batch_ca_logistic_regression(MPI_Comm,
Options<T>, int*, int*, T*, T*, T*);

void mini_batch_ca_logistic_regression_mkl(MPI_Comm comm, Options<float> opt,
int* rowptr, int* colptr, float* data, float* labels, float* weights, int nthreads);

void scale_gram_matrix(unsigned int ldGram, float* labels_row, float* labels_col, float* gram_matrix);
// Single-threaded linear-algebra routine.
template <typename T>
void sparse_axpy(int, T, int*, int*, T*, T*);
template <typename T>
void sparse_scal(int, T, int*, int*, T*, T*);
template <typename T>
void sparse_dot(int, int*, int*, T*, T*, T*);
template <typename T>
void sparse_gemv(unsigned int, int*, T, int*, int*, T*, T*, T*);
template <typename T>
void sparse_gemv_transposed(unsigned int, int*, T, int*, int*, T*, T*, T*);
template <typename T>
void axpy(unsigned int, T, T*, T*);
template <typename T>
void scal(unsigned int, T, T*);
template <typename T>
void dot(unsigned int, T*, T*, T*);
template <typename T>
void dot_unrolled(unsigned int, T*, T*, T*);
template <typename T>
void dense_gemv(unsigned int, T, T*, T*, T*, T*, T*);
template <typename T>
void rowmaj_dense_gemv(unsigned int, T, T*, T*, T*, T*, T*);
template <typename T>
void gemv(unsigned int, T, int*, int*, T*, T*, T*);
template<typename T>
void compute_gram_matrix(unsigned int, int*, T, int*, int*, T*, T*);
template <typename T>
void blocked_compute_gram_matrix(unsigned int, unsigned int, int*,
T, int*, int*, T*, T*);
template <typename T>
void batched_compute_gram_matrix(unsigned int, unsigned int, int*,
T, int*, int*, T*, T*);
template <typename T>
void sampled_csr_gemm(unsigned int, int*,
unsigned int, int*, T, int*, int*, T*, T*);
#endif  // INCLUDE_LOGISTIC_REGRESSION_MPI_HPP_
