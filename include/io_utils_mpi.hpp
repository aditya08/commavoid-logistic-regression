// Copyright 2019 Aditya Devarakonda. All rights reserved.
#ifndef INCLUDE_IO_UTILS_MPI_HPP_
#define INCLUDE_IO_UTILS_MPI_HPP_

#include <mpi.h>
/** \struct Read_Statistics
 * \brief Struct which contains key statistics about the
 * CSR matrix along with it's distribution between processors.
 */
struct Read_Statistics {
  //  per-rank stats.
  /**
   * number of rows of the matrix
   * stored on this rank. 
  */
  unsigned int nrows;
  /**
   * number of columns of the matrix
   * stored on this rank. 
  */
  unsigned int ncols;
  /**
   * number of non-zeros of the matrix
   * stored on this rank. 
  */
  unsigned int nnz;
  //  cummulative stats.
  /**
   * number of rows in the matrix.
  */
  unsigned int nrows_total;
  /**
   * number of columns in the matrix.
  */
  unsigned int ncols_total;
  /**
   * number of non-zeros in the matrix.
  */
  unsigned int nnz_total;
};
/**
 * \brief Parallel read for libsvm with the results finally stored in 
 * 1D block column layout such that each processor stores an equal 
 * number of columns.
*/
template <typename T>
Read_Statistics parallel_read_static_lb(MPI_Comm, const char*,
int**, int**, T**, T**);
/**
 * \brief Parallel read for libsvm with the results finally stored in
 * 1D block column layout such that each processor stores an equal
 * number of non-zeros.
 */
template <typename T>
Read_Statistics parallel_read(MPI_Comm, const char*, int**, int**, T**, T**);
/**
 * \brief Parallel write of an input CSR matrix. This routine is primarily for
 * debugging purposes.
 */
template <typename T>
void parallel_write(MPI_Comm, const char*, Read_Statistics, int*, int*, T*, T*);

#endif  // INCLUDE_IO_UTILS_MPI_HPP_
