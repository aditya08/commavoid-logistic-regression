// Copyright 2019 Aditya Devarakonda. All rights reserved.
#ifndef INCLUDE_LOGISTIC_REGRESSION_CPU_HPP_
#define INCLUDE_LOGISTIC_REGRESSION_CPU_HPP_

#include <vector>
#include <string>

template <typename T>
struct Options {
  T tol;  // convergence tolerance.
  T eta;  // learning rate.
  unsigned int nrows;
  unsigned int ncols;
  unsigned int nnz;
  unsigned int seed;  // rng seed.
  unsigned int max_iters;  // maximum number of iterations.
  unsigned int s;  // communication-avoiding parameter.
  unsigned int chkpt_freq;  // checkpointing frequency.
  unsigned int log_freq;  // logging or printing frequency
  unsigned int batch_size;  // batch size for SGD.
  // selects the optimizer: SGD or CA-SGD (case-insensitive).
  std::string optimizer;
  // the filepath where to store the log. Default: ./logistic_regression.log
  std::string logfile_path;
  // the filepath where to store the model. Default: ./logistic_regression.model
  std::string modelfile_path;
  // the filepath where to store benchmark/timing information.
  // Default: ./logistic_regression.timing
  std::string timerfile_path;
};

template <typename T>
T training_loss(unsigned int, int*, int*, T*, T*, T*);
template <typename T>
T training_accuracy(unsigned int, int*, int*, T*, T*, T*);

template <typename T>
void logistic_regression(Options<T>, int*, int*, T*, T*, T*);

template <typename T>
void ca_logistic_regression(Options<T>, int*, int* , T*, T*, T*);

template <typename T>
void sparse_axpy(int, T, int*, int*, T*, T*);
template <typename T>
void sparse_scal(int, T, int*, int*, T*, T*);
template <typename T>
void sparse_dot(int, int*, int*, T*, T*, T*);

template <typename T>
void sparse_gemv(unsigned int, int*, T, int*, int*, T*, T*, T*);

template <typename T>
void axpy(unsigned int, T, T*, T*);
template <typename T>
void scal(unsigned int, T, T*);
template <typename T>
void dot(unsigned int, T*, T*, T*);
template <typename T>
void dot_unrolled(unsigned int, T*, T*, T*);
template <typename T>
void gemv(unsigned int, T, int*, int*, T*, T*, T*);
template<typename T>
void compute_gram_matrix(
unsigned int, int* gramidxs, T alpha, int*, int*, T*, T*);

#endif  // INCLUDE_LOGISTIC_REGRESSION_CPU_HPP_
