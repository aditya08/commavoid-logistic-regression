// Copyright 2019 Aditya Devarakonda. All rights reserved.
#ifndef INCLUDE_GENERATE_DATA_HPP_
#define INCLUDE_GENERATE_DATA_HPP_

#include <mpi.h>

// generate a sparse CSR matrix on each rank.
template <typename T>
void random_csr_per_rank(MPI_Comm,
int, int, T, int*, int*, T*, T*, bool rows_per_rank = false);

#endif  // INCLUDE_GENERATE_DATA_HPP_
