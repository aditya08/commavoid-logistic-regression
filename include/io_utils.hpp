// Copyright 2019 Aditya Devarakonda. All rights reserved.
#ifndef INCLUDE_IO_UTILS_HPP_
#define INCLUDE_IO_UTILS_HPP_
#include <vector>
struct Read_Statistics {
    unsigned int nrows;
    unsigned int ncols;
    unsigned int nnz;
};
template <typename T>
Read_Statistics read(const char*, int**, int**, T**, T**);
template <typename T>
void write(const char*, Read_Statistics, int*, int*, T*, T*);

#endif  // INCLUDE_IO_UTILS_HPP_
