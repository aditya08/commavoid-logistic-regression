// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>
#include <cstring>

#include "utils.hpp"
#include "io_utils_mpi.hpp"
#include "logistic_regression_mpi.hpp"


int main(int argc, char** argv) {
  if (argc < 4) {
        std::cout << "No enough arguments." << std::endl;
        std::cout << "Usage: " << argv[0] << "[Filename]" << std::endl;
  }
  MPI_Init(&argc, &argv);
  MPI_Comm comm;
  int mpi_rank, mpi_size;
  MPI_Comm_dup(MPI_COMM_WORLD, &comm);
  MPI_Comm_rank(comm, &mpi_rank);
  MPI_Comm_size(comm, &mpi_size);
  // read libsvm matrix from file.
  int *rowptr = nullptr, *colptr = nullptr;
  double *data = nullptr, *labels = nullptr;

  Read_Statistics rd_stats = parallel_read_static_lb(comm, argv[1],
  &rowptr, &colptr, &data, &labels);

  if (rd_stats.nrows == 0) {
    std::cerr << "libsvm read() failed!" << std::endl;
    return -1;
  }

  unsigned int s = std::stoi(argv[2]);
  unsigned int batch_size = std::stoi(argv[3]);
  unsigned int gram_nvals = (((s-1)*s)/2)*batch_size*batch_size;
  int* gramidxs = static_cast<int*>(ALIGNED_ALLOC(MEM_ALIGN,
  sizeof(int)*s*batch_size));

  for (unsigned int i = 0; i < s*batch_size; ++i) {
    gramidxs[i] = i % rd_stats.nrows_total;
    std::cout << gramidxs[i] << " ";
    if ((i+1) % batch_size == 0) {
      std::cout << std::endl;
    }
  }

  double* gram_matrix = static_cast<double*>(ALIGNED_ALLOC(MEM_ALIGN,
  sizeof(double)*gram_nvals));

  // test Block Compute Gram Matrix
  std::cout << "s: " << s << ", batch_size: " << batch_size << std::endl;
  std::cout << "Calling blocked_compute_gram_matrix()..." << std::endl;
  blocked_compute_gram_matrix(s, batch_size, gramidxs, 1.,
  rowptr, colptr, data, gram_matrix);
  for (unsigned int i = 0; i < gram_nvals; ++i) {
    std::cout << gram_matrix[i] << " ";
    if ((i+1) % batch_size == 0) {
      std::cout << std::endl;
    }
    if ((i+1) % (batch_size*batch_size) == 0) {
      std::cout << std::endl;
    }
  }
  std::cout << std::endl;

  std::cout << "Done." << std::endl;
  ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr), ALIGNED_FREE(data);
  ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gramidxs);
  MPI_Finalize();
  return 0;
}
