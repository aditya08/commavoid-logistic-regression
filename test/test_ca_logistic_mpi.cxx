// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <mpi.h>

#include <iostream>
#include <iomanip>
#include <vector>
// TODO(Aditya): replace with a c++11 compliant library.
#include <chrono>
#include <cstdlib>
#include <cstring>

#include "utils.hpp"
#include "io_utils_mpi.hpp"
#include "logistic_regression_mpi.hpp"

int main(int argc, char** argv) {
  if (argc < 6) {
        std::cout << "No enough arguments." << std::endl;
        std::cout << "Usage: " << argv[0] << "[Filename]" << std::endl;
  }
  MPI_Init(&argc, &argv);
  MPI_Comm comm;
  int mpi_rank, mpi_size;
  MPI_Comm_dup(MPI_COMM_WORLD, &comm);
  MPI_Comm_rank(comm, &mpi_rank);
  MPI_Comm_size(comm, &mpi_size);
  int recv_buf;
  int* rowptr = nullptr;
  int* colptr = nullptr;
  double* data = nullptr;
  double* labels = nullptr;
  unsigned int ntrials = std::stoi(argv[4]);
  unsigned int nepochs = std::stoi(argv[3]);
  int nnz_lb = std::stoi(argv[5]);
  Read_Statistics rd_stats;
  if (nnz_lb) {
    if (mpi_rank == 0) {
      std::cout << "Using nnz balancing." << std::endl;
    }
    MPI_Barrier(comm);
    rd_stats = parallel_read(comm, argv[1],
    &rowptr, &colptr, &data, &labels);
  } else {
    if (mpi_rank == 0) {
      std::cout << "Using col balancing." << std::endl;
    }
    MPI_Barrier(comm);
    rd_stats = parallel_read_static_lb(comm, argv[1],
    &rowptr, &colptr, &data, &labels);
  }
  if (mpi_rank == 0) {
    std::cout << "Matrix size is " << rd_stats.nrows_total << " rows by "
    << rd_stats.ncols_total << " columns." << std::endl;
    std::cout << "nnz = " << rd_stats.nnz_total << " density = "
    << static_cast<double>(rd_stats.nnz_total)/rd_stats.nrows_total/
    rd_stats.ncols_total << std::endl;
  }
  if (mpi_size > 1 && mpi_rank == 0) {
    std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
    << " rows " << rd_stats.ncols << " columns "
    << rd_stats.nnz << " nnz" << std::endl;
    MPI_Send(&rd_stats.ncols, 1, MPI_INT, mpi_rank+1, 0, comm);
  } else if (mpi_size > 1 && mpi_rank == mpi_size - 1) {
    MPI_Recv(&recv_buf, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG,
    comm, MPI_STATUS_IGNORE);
    recv_buf += rd_stats.ncols;
    std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
    << " rows " << rd_stats.ncols << " columns "
    << rd_stats.nnz << " nnz" << std::endl;
    std::cout << "Total ncols: " << recv_buf << std::endl;
  } else if (mpi_size > 1) {
    MPI_Recv(&recv_buf, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG,
    comm, MPI_STATUS_IGNORE);
    recv_buf += rd_stats.ncols;
    std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
    << " rows " << rd_stats.ncols << " columns "
    << rd_stats.nnz << " nnz" << std::endl;
    MPI_Send(&recv_buf, 1, MPI_INT, mpi_rank+1, 0, comm);
  }
  MPI_Barrier(comm);
  Options<double> opt;
  opt.max_iters = nepochs*rd_stats.nrows;  // 50 epochs of training.
  opt.s = std::stoi(argv[2]);
  opt.tol = 1e-8;
  opt.eta = 1e1;
  opt.seed = 100;
  opt.chkpt_freq = 1000;
  opt.log_freq = rd_stats.nrows_total;
  opt.batch_size = 1;
  opt.optimizer = "CA-SGD";
  opt.nrows = rd_stats.nrows;
  opt.nnz = rd_stats.nnz;
  opt.ncols = rd_stats.ncols;
  double* weights = static_cast<double*>(ALIGNED_ALLOC(MEM_ALIGN,
  sizeof(double)*rd_stats.ncols));
  // use std::chorno::steady_clock to time each ca_logistic_regression(.) call.
  auto start = std::chrono::steady_clock::now();
  auto end = std::chrono::steady_clock::now();
  auto diff = end - start;
  // use std::chorno::steady_clock to time each ca_logistic_regression(.) call.
  std::chrono::duration<double> ca_average =
  std::chrono::duration<double>::zero();
  std::vector<std::chrono::duration<double>> ca_elapsed_times;
  std::chrono::duration<double> ca_min_elapsed_time =
  std::chrono::duration<double>::max();
  std::chrono::duration<double> ca_max_elapsed_time =
  std::chrono::duration<double>::min();
  if (mpi_rank == 0) {
    std::cout << "s = " << opt.s << std::endl << std::endl;
  }
  MPI_Barrier(comm);
  // repeat calls for niters.

  for (unsigned int i = 1; i < ntrials+1; ++i) {
      start = std::chrono::steady_clock::now();
      ca_logistic_regression(comm, opt, rowptr, colptr, data, labels, weights);
      end = std::chrono::steady_clock::now();
      diff = end - start;
      if (diff < ca_min_elapsed_time) {
          ca_min_elapsed_time = diff;
      }
      if (diff > ca_max_elapsed_time) {
          ca_max_elapsed_time = diff;
      }
      ca_average += diff;
      ca_elapsed_times.push_back(diff);
      if (mpi_rank == 0) {
        std::cout << "training logistic regression elapsed time: "
        << std::chrono::duration<double, std::milli>
        (diff).count() << " ms" << std::endl << std::endl;
      }
  }
  // print CA logistic regression elapsed time statistics.
  if (mpi_rank == 0) {
    std::cout << "training CA-logistic regression average elapsed time: "
    << std::chrono::duration<double, std::milli>
    (ca_average).count()/ntrials << " ms" << std::endl;
    std::cout << "training CA-logistic regression max elapsed time: "
    << std::chrono::duration<double, std::milli>
    (ca_max_elapsed_time).count() << " ms" << std::endl;
    std::cout << "training CA-logistic regression min elapsed time: "
    << std::chrono::duration<double, std::milli>
    (ca_min_elapsed_time).count() << " ms" << std::endl;
  }
  ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr), ALIGNED_FREE(data);
  ALIGNED_FREE(labels), ALIGNED_FREE(weights);
  MPI_Finalize();
  return 0;
}
