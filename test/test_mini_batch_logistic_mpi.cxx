//  Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <mpi.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <chrono>

#include "utils.hpp"
#include "io_utils_mpi.hpp"
#include "logistic_regression_mpi.hpp"

int main(int argc, char** argv) {
  if (argc < 5) {
        std::cout << "No enough arguments." << std::endl;
        std::cout << "Usage: " << argv[0] << "[Filename]" << std::endl;
  }
  MPI_Init(&argc, &argv);
  MPI_Comm comm;
  int mpi_size, mpi_rank;
  int recv_buf;
  MPI_Comm_dup(MPI_COMM_WORLD, &comm);
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);
  // malloc'd inside read.
  int* rowptr;
  int* colptr;
  double* data;
  double* labels;
  int batch_size = std::stoi(argv[2]);
  unsigned int nepochs = std::stoi(argv[3]);
  int nnz_lb = std::stoi(argv[5]);
  Read_Statistics rd_stats;
  if (nnz_lb) {
    if (mpi_rank == 0) {
      std::cout << "Using nnz balancing." << std::endl;
    }
    MPI_Barrier(comm);
    rd_stats = parallel_read(comm, argv[1],
    &rowptr, &colptr, &data, &labels);
  } else {
    if (mpi_rank == 0) {
      std::cout << "Using col balancing." << std::endl;
    }
    MPI_Barrier(comm);
    rd_stats = parallel_read_static_lb(comm, argv[1],
    &rowptr, &colptr, &data, &labels);
  }
  if (mpi_rank == 0) {
    std::cout << "Matrix size is " << rd_stats.nrows_total << " rows by "
    << rd_stats.ncols_total << " columns." << std::endl;
    std::cout << "nnz = " << rd_stats.nnz_total << " density = "
    << static_cast<double>(rd_stats.nnz_total)/rd_stats.nrows_total/
    rd_stats.ncols_total << std::endl;
    std::cout << "Rank 0: Matrix size is " << rd_stats.nrows
    << " rows by " << rd_stats.ncols << " columns." << std::endl;
    std::cout << "nnz = " << rd_stats.nnz << " density = "
    << static_cast<double>(rd_stats.nnz)/rd_stats.nrows/
    rd_stats.ncols << std::endl;
  }
  if (mpi_size > 1 && mpi_rank == 0) {
    std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
    << " rows " << rd_stats.ncols << " columns "
    << rd_stats.nnz << " nnz" << std::endl;
    MPI_Send(&rd_stats.ncols, 1, MPI_INT, mpi_rank+1, 0, comm);
  } else if (mpi_size > 1 && mpi_rank == mpi_size - 1) {
    MPI_Recv(&recv_buf, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG,
    comm, MPI_STATUS_IGNORE);
    recv_buf += rd_stats.ncols;
    std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
    << " rows " << rd_stats.ncols << " columns "
    << rd_stats.nnz << " nnz" << std::endl;
    std::cout << "Total ncols: " << recv_buf << std::endl;
  } else if (mpi_size > 1) {
    MPI_Recv(&recv_buf, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG,
    comm, MPI_STATUS_IGNORE);
    recv_buf += rd_stats.ncols;
    std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
    << " rows " << rd_stats.ncols << " columns "
    << rd_stats.nnz << " nnz" << std::endl;
    MPI_Send(&recv_buf, 1, MPI_INT, mpi_rank+1, 0, comm);
  }
  MPI_Barrier(comm);
  Options<double> opt;
  opt.max_iters = nepochs*rd_stats.nrows;
  opt.s = 1;
  opt.tol = 1e-8;
  opt.eta = 1e1;
  opt.seed = 100;
  opt.chkpt_freq = 1000;
  opt.log_freq = rd_stats.nrows;
  opt.batch_size = batch_size;
  opt.optimizer = "CA-SGD";
  opt.nrows = rd_stats.nrows;  // same as nrows_total
  opt.nnz = rd_stats.nnz;  // this rank's nnz.
  opt.ncols = rd_stats.ncols;  // this rank's ncols.
  unsigned int ntrials = std::stoi(argv[4]);
  double* weights = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*rd_stats.ncols));
  // use std::chorno::steady_clock to time each ca_logistic_regression(.) call.
  std::chrono::duration<double> average =
  std::chrono::duration<double>::zero();
  std::vector<std::chrono::duration<double>> elapsed_times;
  std::chrono::duration<double> min_elapsed_time =
  std::chrono::duration<double>::max();
  std::chrono::duration<double> max_elapsed_time =
  std::chrono::duration<double>::min();
  auto start = std::chrono::steady_clock::now();
  auto end = std::chrono::steady_clock::now();
  auto diff = end - start;
  if (mpi_rank == 0) {
    std::cout << "starting logistic_regression runs..." << std::endl;
  }
  // repeat calls for niters.
  for (unsigned int i = 1; i < ntrials+1; ++i) {
      start = std::chrono::steady_clock::now();
      mini_batch_logistic_regression(comm, opt, rowptr,
      colptr, data, labels, weights);
      end = std::chrono::steady_clock::now();
      diff = end - start;
      if (diff < min_elapsed_time) {
          min_elapsed_time = diff;
      }
      if (diff > max_elapsed_time) {
          max_elapsed_time = diff;
      }
      elapsed_times.push_back(diff);
      average += diff;
      if (mpi_rank == 0) {
        std::cout << "training logistic regression elapsed time: "
        << std::chrono::duration<double, std::milli>
        (diff).count() << " ms" << std::endl << std::endl;
      }
  }
  // print logistic regression elapsed time statistics.
  if (mpi_rank == 0) {
    std::cout << "training logistic regression average elapsed time: "
    << std::chrono::duration<double, std::milli>
    (average).count()/ntrials << " ms" << std::endl;
    std::cout << "training logistic regression max elapsed time: "
    << std::chrono::duration<double, std::milli>
    (max_elapsed_time).count() << " ms" << std::endl;
    std::cout << "training logistic regression min elapsed time: "
    << std::chrono::duration<double, std::milli>
    (min_elapsed_time).count() << " ms" << std::endl << std::endl;
  }
  //  repeat calls for niters.
  ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr), ALIGNED_FREE(data);
  ALIGNED_FREE(labels), ALIGNED_FREE(weights);

  MPI_Finalize();
  return 0;
}
