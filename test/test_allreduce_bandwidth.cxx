// Copyright 2019 Aditya Devarakonda
#include <mpi.h>

#include <iostream>
#include <ctime>
#include <string>

#include "utils.hpp"

int main(int argc, char* argv[]) {
  if (argc < 4) {
    std::cout << argv[0] << " [batch_size] [s] [number of trials]" << std::endl;
    return -1;
  }

  MPI_Init(&argc, &argv);
  int mpi_rank, mpi_size;

  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
  double *send_buffer, *recv_buffer;
  unsigned int nvals = std::stoi(argv[1]);
  unsigned int s = std::stoi(argv[2]);
  unsigned int ntrials = std::stoi(argv[3]);
  send_buffer = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*nvals));
  recv_buffer = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*nvals));

  double *send_ca_buffer, *recv_ca_buffer;
  unsigned int ca_nvals = (s*(s-1)/2)*nvals*nvals + s*nvals;
  send_ca_buffer = static_cast<double*>
                  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*ca_nvals));
  recv_ca_buffer = static_cast<double*>
                  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*ca_nvals));

  if (send_buffer == nullptr || recv_buffer == nullptr ||
      send_ca_buffer == nullptr || recv_ca_buffer == nullptr) {
    std::cerr << "Call to ALIGNED_ALLOC failed!" << std::endl;
  }
  for (unsigned int i = 0; i < nvals; ++i) {
    send_buffer[i] = 1.*std::rand()/RAND_MAX;
  }
  for (unsigned int i = 0; i < ca_nvals; ++i) {
    send_ca_buffer[i] = 1.*std::rand()/RAND_MAX;
  }
  double start = MPI_Wtime();
  for (unsigned int i = 0; i < ntrials; ++i) {
    MPI_Allreduce(send_buffer, recv_buffer, nvals,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  }
  double end = MPI_Wtime()-start;
  double time_avg;
  MPI_Allreduce(&end, &time_avg, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  time_avg /= static_cast<double>(mpi_size);
  if (mpi_rank == 0) {
    std::cout << "Average elapsed time: " << time_avg << " sec." << std::endl;
    std::cout << "MPI_Allreduce bandwidth: "
    << (static_cast<double>(sizeof(double)*nvals)/1000)/time_avg/ntrials
    << " KB/s" << std::endl;
  }
  ALIGNED_FREE(send_buffer), ALIGNED_FREE(recv_buffer);
  start = MPI_Wtime();
  for (unsigned int i = 0; i < ntrials/s; ++i) {
    MPI_Allreduce(send_ca_buffer, recv_ca_buffer, ca_nvals,
    MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  }
  end = MPI_Wtime() - start;
  MPI_Allreduce(&end, &time_avg, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  time_avg /= static_cast<double>(mpi_size);
  if (mpi_rank == 0) {
    std::cout << "Average CA-variant elapsed time: "
    << time_avg << " sec." << std::endl;
    std::cout << "CA-variant MPI_Allreduce bandwidth: "
    << (static_cast<double>(sizeof(double)*ca_nvals)/1000)/time_avg/ntrials
    << " KB/s" << std::endl;
  }
  ALIGNED_FREE(send_ca_buffer), ALIGNED_FREE(recv_ca_buffer);
  MPI_Finalize();
  return 0;
}
