// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <cstring>

#include "utils.hpp"
#include "io_utils.hpp"
#include "logistic_regression_cpu.hpp"

int main(int argc, char** argv) {
  if (argc < 4) {
        std::cout << "No enough arguments." << std::endl;
        std::cout << "Usage: " << argv[0] << "[Filename]" << std::endl;
    }
  int* rowptr = nullptr;
  int* colptr = nullptr;
  double* data = nullptr;
  double* labels = nullptr;
  unsigned int ntrials = std::stoi(argv[3]);
  unsigned int nepochs = std::stoi(argv[2]);
  Read_Statistics rd_stats = read(argv[1], &rowptr, &colptr, &data, &labels);
  std::cout << "Matrix size is " << rd_stats.nrows << " rows by "
  << rd_stats.ncols << " columns." << std::endl;
  std::cout << "nnz = " << rd_stats.nnz << " density = "
  << static_cast<double>(rd_stats.nnz)/rd_stats.nrows/rd_stats.ncols
  << std::endl;
  Options<double> opt;
  opt.max_iters = nepochs*rd_stats.nrows;  // 50 epochs of training.
  opt.s = 1;
  opt.tol = 1e-8;
  opt.eta = 1e1;
  opt.seed = 100;
  opt.chkpt_freq = 1000;
  opt.log_freq = rd_stats.nrows;
  opt.batch_size = 1;
  opt.optimizer = "CA-SGD";
  opt.nrows = rd_stats.nrows;
  opt.nnz = rd_stats.nnz;
  opt.ncols = rd_stats.ncols;

  double* weights = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*rd_stats.ncols));
  std::chrono::duration<double> average = std::chrono::duration<double>::zero();
  std::vector<std::chrono::duration<double>> elapsed_times;
  std::chrono::duration<double> min_elapsed_time =
  std::chrono::duration<double>::max();
  std::chrono::duration<double> max_elapsed_time =
  std::chrono::duration<double>::min();

  auto start = std::chrono::steady_clock::now();
  auto end = std::chrono::steady_clock::now();
  auto diff = end - start;
  std::cout << "starting logistic_regression runs..." << std::endl;
  // repeat calls for niters.
  for (unsigned int i = 1; i < ntrials+1; ++i) {
      start = std::chrono::steady_clock::now();
      logistic_regression(opt, rowptr, colptr, data, labels, weights);
      end = std::chrono::steady_clock::now();
      diff = end - start;
      if (diff < min_elapsed_time) {
          min_elapsed_time = diff;
      }
      if (diff > max_elapsed_time) {
          max_elapsed_time = diff;
      }
      elapsed_times.push_back(diff);
      average += diff;
      std::cout << "training logistic regression elapsed time: "
      << std::chrono::duration<double, std::milli>
      (diff).count() << " ms" << std::endl << std::endl;
  }
  // print logistic regression elapsed time statistics.
  std::cout << "training logistic regression average elapsed time: "
  << std::chrono::duration<double, std::milli>
  (average).count()/ntrials << " ms" << std::endl;
  std::cout << "training logistic regression max elapsed time: "
  << std::chrono::duration<double, std::milli>
  (max_elapsed_time).count() << " ms" << std::endl;
  std::cout << "training logistic regression min elapsed time: "
  << std::chrono::duration<double, std::milli>
  (min_elapsed_time).count() << " ms" << std::endl << std::endl;
  ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr), ALIGNED_FREE(data);
  ALIGNED_FREE(labels), ALIGNED_FREE(weights);
  return 0;
}
