// Copyright 2019 Aditya Devarakonda
#include <vector>
#include <iostream>
#include <chrono>
#include <string>
#include <cstdlib>
#include <ctime>
#include <iomanip>

#include "utils.hpp"
#include "logistic_regression_cpu.hpp"

int main(int argc, char* argv[]) {
  if (argc < 4) {
      std::cout << argv[0] << " [vector length] [number of trails] [seed]"
      << std::endl;
      return -1;
  }
  double* vec_aligned_a, *vec_aligned_b;
  unsigned int nvals = std::stoi(argv[1]);
  unsigned int ntrials = std::stoi(argv[2]);
  unsigned int seed = std::stoi(argv[3]);
  std::vector<double> vec_a(nvals);
  std::vector<double> vec_b(nvals);
  std::vector<double> vec_c(nvals);
  std::vector<double> vec_d(nvals);
  vec_aligned_a = static_cast<double *>(ALIGNED_ALLOC(MEM_ALIGN,
  sizeof(double)*nvals));
  vec_aligned_b = static_cast<double *>(ALIGNED_ALLOC(MEM_ALIGN,
  sizeof(double)*nvals));
  if (vec_aligned_a == nullptr || vec_aligned_b == nullptr) {
      std::cerr << "Call to posix_memalign failed!" << std::endl;
      return -1;
  }
  std::srand(seed);
  for (unsigned int i = 0; i < nvals; ++i) {
      // vectors for naive.
      vec_a[i] = 1.*std::rand()/RAND_MAX;
      vec_b[i] = 1.*std::rand()/RAND_MAX;
      // vectors for optimized.
      vec_c[i] = vec_a[i];
      vec_d[i] = vec_b[i];

      vec_aligned_a[i] = vec_a[i];
      vec_aligned_b[i] = vec_b[i];
  }

  double dotprod = 0.;
  auto start = std::chrono::steady_clock::now();
  auto end = std::chrono::steady_clock::now();
  auto diff = std::chrono::duration<double>::zero();
  for (unsigned int i = 0; i < ntrials; ++i) {
      dotprod = 0.;
      start = std::chrono::steady_clock::now();
      dot(nvals, vec_aligned_a, vec_aligned_b, &dotprod);
      end = std::chrono::steady_clock::now();
      diff += end - start;
  }
  const unsigned int WORDS_MOVED = static_cast<unsigned int>
                                    (sizeof(double)*nvals*2.);
  std::cout << "dot result: " << std::setprecision(15) << dotprod << std::endl;
  std::cout << "words moved: " << WORDS_MOVED/1000 << " KB" << std::endl;
  std::cout << "dot time: " << std::chrono::duration<double,
  std::milli>(diff).count()/ntrials << " ms" << std::endl;
  std::cout << "bandwidth: " << WORDS_MOVED/
  (std::chrono::duration<double>(diff).count()/ntrials)/1000/1000
  << " MB/s" << std::endl << std::endl;
  diff = std::chrono::duration<double>::zero();
  double dotprod2 = 0.;
  for (unsigned int i = 0; i < ntrials; ++i) {
      dotprod2 = 0.;
      start = std::chrono::steady_clock::now();
      dot_unrolled(nvals, vec_aligned_a, vec_aligned_b, &dotprod2);
      end = std::chrono::steady_clock::now();
      diff += end - start;
  }

  std::cout << "dot result: " << std::setprecision(15) << dotprod << std::endl;
  std::cout << "dot_unrolled result: " << std::setprecision(15)
  << dotprod2 << std::endl;
  std::cout << "dot difference : " << dotprod - dotprod2 << std::endl;
  std::cout << "words moved: " << WORDS_MOVED/1000 << " KB" << std::endl;
  std::cout << "dot time: " << std::chrono::duration<double, std::milli>
  (diff).count()/ntrials << " ms" << std::endl;
  std::cout << "bandwidth: " << WORDS_MOVED/
  (std::chrono::duration<double>(diff).count()/ntrials)/1000/1000
  << " MB/s" << std::endl;
  ALIGNED_FREE(vec_aligned_a), ALIGNED_FREE(vec_aligned_b);
  return 0;
}
