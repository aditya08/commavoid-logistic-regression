// Copyright 2019 Aditya Devarakonda. All rights reserved.

#include <iomanip>
#include <iostream>
#include <vector>
// TODO(Aditya): replace with a c++11 compliant library.
#include <mkl.h>
#include <mpi.h>

#include <cstdlib>
#include <cstring>

#include "io_utils_mpi.hpp"
#include "logistic_regression_mkl_mpi.hpp"
#include "utils.hpp"

int main(int argc, char** argv) {
    if (argc < 6) {
        std::cout << "No enough arguments." << std::endl;
        std::cout << "Usage: " << argv[0]
                            << " [Filename] [s] [nepochs] [ntrials] [1 if nnz_load_balancing "
                                 "or 0 if col_balancing] [nthreads] [batch-size]"
                            << std::endl;
    }
    MPI_Init(&argc, &argv);
    MPI_Comm comm;
    int mpi_rank, mpi_size;
    MPI_Comm_dup(MPI_COMM_WORLD, &comm);
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    int recv_buf;
    int* rowptr = nullptr;
    int* colptr = nullptr;
    float* data = nullptr;
    float* labels = nullptr;
    unsigned int nepochs = std::stoi(argv[3]);
    unsigned int ntrials = std::stoi(argv[4]);
    int nnz_lb = std::stoi(argv[5]);
    int nthreads = std::stoi(argv[6]);
    Read_Statistics rd_stats;
    if (nnz_lb) {
        if (mpi_rank == 0) {
            std::cout << "Using nnz balancing." << std::endl;
        }
        MPI_Barrier(comm);
        rd_stats = parallel_read(comm, argv[1], &rowptr, &colptr, &data, &labels);
    } else {
        if (mpi_rank == 0) {
            std::cout << "Using col balancing." << std::endl;
        }
        MPI_Barrier(comm);
        rd_stats = parallel_read_static_lb(comm, argv[1], &rowptr, &colptr, &data,
                                                                             &labels);
    }
    if (mpi_rank == 0) {
        std::cout << "Matrix size is " << rd_stats.nrows_total << " rows by "
                            << rd_stats.ncols_total << " columns." << std::endl;
        std::cout << "nnz = " << rd_stats.nnz_total << " density = "
                            << static_cast<float>(rd_stats.nnz_total) / rd_stats.nrows_total /
                                         rd_stats.ncols_total
                            << std::endl;
    }
    if (mpi_size > 1 && mpi_rank == 0) {
        std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
                            << " rows " << rd_stats.ncols << " columns " << rd_stats.nnz
                            << " nnz" << std::endl;
        MPI_Send(&rd_stats.ncols, 1, MPI_INT, mpi_rank + 1, 0, comm);
    } else if (mpi_size > 1 && mpi_rank == mpi_size - 1) {
        MPI_Recv(&recv_buf, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG, comm,
                         MPI_STATUS_IGNORE);
        recv_buf += rd_stats.ncols;
        std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
                            << " rows " << rd_stats.ncols << " columns " << rd_stats.nnz
                            << " nnz" << std::endl;
        std::cout << "Total ncols: " << recv_buf << std::endl;
    } else if (mpi_size > 1) {
        MPI_Recv(&recv_buf, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG, comm,
                         MPI_STATUS_IGNORE);
        recv_buf += rd_stats.ncols;
        std::cout << "[Rank " << mpi_rank << "]: matrix size " << rd_stats.nrows
                            << " rows " << rd_stats.ncols << " columns " << rd_stats.nnz
                            << " nnz" << std::endl;
        MPI_Send(&recv_buf, 1, MPI_INT, mpi_rank + 1, 0, comm);
    }
    MPI_Barrier(comm);
    Options<float> opt;
    opt.max_iters = nepochs * rd_stats.nrows;  // 50 epochs of training.
    opt.s = std::stoi(argv[2]);
    opt.tol = 1e-8;
    opt.eta = 1e1;
    opt.seed = 100;
    opt.chkpt_freq = 1000;
    opt.log_freq = rd_stats.nrows_total;
    opt.batch_size = std::stoi(argv[7]);
    opt.optimizer = "CA-SGD";
    opt.nrows = rd_stats.nrows;
    opt.nnz = rd_stats.nnz;
    opt.ncols = rd_stats.ncols;
    float* weights = static_cast<float*>(
            ALIGNED_ALLOC(MEM_ALIGN, sizeof(float) * rd_stats.ncols));
    if (mpi_rank == 0) {
        std::cout << "s = " << opt.s << std::endl << std::endl;
    }
    MPI_Barrier(comm);
    // repeat calls for niters.
    double nonmkl_elapsed = 0., start = 0., nonmkl_time;
    for (unsigned int i = 1; i < ntrials + 1; ++i) {
        start = dsecnd();
        //   ca_logistic_regression_mkl(comm, opt, rowptr, colptr, data, labels,
        //   weights, nthreads);
        // mini_batch_ca_logistic_regression(comm, opt, rowptr, colptr, data, labels, weights);
        mini_batch_ca_logistic_regression(comm, opt, rowptr, colptr, data, labels, weights);
        // ca_logistic_regression(comm, opt, rowptr, colptr, data, labels, weights);
        nonmkl_time = dsecnd() - start;
        nonmkl_elapsed += nonmkl_time;
        if (mpi_rank == 0) {
            std::cout << "training s-step sgd logistic regression elapsed time: "
                                << nonmkl_time * 1000 << " ms" << std::endl
                                << std::endl;
        }
    }

    double mkl_elapsed = 0., mkl_time = 0.;
    for (unsigned int i = 1; i < ntrials + 1; ++i) {
        start = dsecnd();
        // mini_batch_ca_logistic_regression_mkl(comm, opt, rowptr, colptr, data, labels, weights, nthreads);
        mini_batch_logistic_regression(comm, opt, rowptr, colptr, data, labels, weights);
        //   ca_logistic_regression_mkl(comm, opt, rowptr, colptr, data, labels,
        //   weights, nthreads);
        // logistic_regression(comm, opt, rowptr, colptr, data, labels, weights);
        mkl_time = dsecnd() - start;
        mkl_elapsed += mkl_time;
        if (mpi_rank == 0) {
            std::cout << "training sgd logistic regression elapsed time: "
                                << mkl_time * 1000 << " ms" << std::endl
                                << std::endl;
        }
    }

    // print CA logistic regression elapsed time statistics.
    if (mpi_rank == 0) {
        std::cout
                << "training s-step sgd logistic regression average elapsed time: "
                << nonmkl_elapsed * 1000 / ntrials << " ms" << std::endl;
        std::cout << "training sgd logistic regression average elapsed time: "
                            << mkl_elapsed * 1000 / ntrials << " ms" << std::endl;
        std::cout << "speedup (sgd / s-step sgd): "
                            << (mkl_elapsed / ntrials) / (nonmkl_elapsed / ntrials)
                            << std::endl;
    }
    ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr), ALIGNED_FREE(data);
    ALIGNED_FREE(labels), ALIGNED_FREE(weights);
    MPI_Finalize();
    return 0;
}
