// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <mpi.h>

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <cstring>

#include "utils.hpp"
#include "io_utils_mpi.hpp"
#include "logistic_regression_mpi.hpp"

int main(int argc, char** argv) {
  if (argc < 2) {
        std::cout << "No enough arguments." << std::endl;
        std::cout << "Usage: " << argv[0] << "[Filename]" << std::endl;
  }

  MPI_Init(&argc, &argv);
  MPI_Comm comm;
  int mpi_rank, mpi_size;

  MPI_Comm_dup(MPI_COMM_WORLD, &comm);
  MPI_Comm_rank(comm, &mpi_rank);
  MPI_Comm_size(comm, &mpi_size);

  //  read libsvm matrix from file.
  auto start = std::chrono::steady_clock::now();
  auto end = std::chrono::steady_clock::now();
  auto diff = end - start;
  int *rowptr = 0, *colptr = 0;
  double *data = 0, *labels = 0;
  Read_Statistics rd_stats =
  parallel_read(comm, argv[1], &rowptr, &colptr, &data, &labels);
  if (rd_stats.nrows == 0) {
    std::cerr << "libsvm read() failed!" << std::endl;
    return -1;
  }
  unsigned int s = std::stoi(argv[2]);
  unsigned int batch_size = std::stoi(argv[3]);
  unsigned int niters = std::stoi(argv[4]);
  unsigned int ngram_vals = (((s*batch_size)-1)*(s*batch_size))/2;
  int* gramidxs = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*s*batch_size));
  double *gram_matrix = static_cast<double*>(ALIGNED_ALLOC(
  MEM_ALIGN, sizeof(double)*ngram_vals));
  unsigned int blocked_ngram_vals2 =
  (((s-1)*s)/2)*batch_size*batch_size;
  double* blocked_gram_matrix2 = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*blocked_ngram_vals2));
  unsigned int goffset = (batch_size*(batch_size-1))/2;
  unsigned int row_offset = 0;
  unsigned int offset = 0;
  for (unsigned int i = 0; i < s*batch_size; ++i) {
    gramidxs[i] = i%rd_stats.nrows;
  }
  diff = std::chrono::duration<int, std::nano>::zero();
  for (unsigned int i = 0; i < niters; ++i) {
    offset = 0;
    row_offset = 0;
    start = std::chrono::steady_clock::now();
    compute_gram_matrix(s*batch_size, gramidxs, 1.,
    rowptr, colptr, data, gram_matrix);
    for (unsigned int l = 0; l < s-1; ++l) {
      row_offset += batch_size;
      for (unsigned int j = 0; j < batch_size; ++j) {
        for (unsigned int k = 0; k < (l+1)*batch_size; ++k) {
          blocked_gram_matrix2[offset] = gram_matrix[(l+1)*goffset
          + ((l*(l+1))/2)*batch_size*batch_size
          + (j)*row_offset + (j*(j-1))/2 + k];
          std::cout << blocked_gram_matrix2[offset] << ' ';
          offset++;
        }
        std::cout << std::endl;
      }
    }
    end = std::chrono::steady_clock::now();
    diff += end - start;
  }

  auto avg_elapsed_time = std::chrono::duration<double, std::milli>
  (diff).count()/niters;
  std::cout << std::endl << "compute_gram_matrix avg elapsed time: "
  << avg_elapsed_time << " ms." << std::endl;

  int* blocked_gramidxs = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*batch_size*s));
  unsigned int blocked_ngram_vals = (((s-1)*s)/2)*batch_size*batch_size;
  double* blocked_gram_matrix = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*blocked_ngram_vals));
  for (unsigned int i = 0; i < s*batch_size; ++i) {
    blocked_gramidxs[i] = i%rd_stats.nrows;
  }
  for (unsigned int i = 0; i < niters; ++i) {
    start = std::chrono::steady_clock::now();
    blocked_compute_gram_matrix(s, batch_size, blocked_gramidxs, 1.
    , rowptr, colptr, data, blocked_gram_matrix);
    end = std::chrono::steady_clock::now();
    diff += end - start;
  }
  avg_elapsed_time = std::chrono::duration<double, std::milli>
  (diff).count()/niters;
  std::cout << std::endl << "blocked_compute_gram_matrix avg elapsed time: "
  << avg_elapsed_time << " ms." << std::endl;

  for (unsigned int i = 0; i < blocked_ngram_vals; ++i) {
    std::cout << blocked_gram_matrix[i] << ' ';
  }

  std::cout << std::endl << std::endl;
  double* out_matrix = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*blocked_ngram_vals));
  for (unsigned int i = 0; i < niters; ++i) {
    start = std::chrono::steady_clock::now();
    batched_compute_gram_matrix(s, batch_size, blocked_gramidxs, 1.
    , rowptr, colptr, data, out_matrix);
    end = std::chrono::steady_clock::now();
    diff += end - start;
  }

  avg_elapsed_time = std::chrono::duration<double, std::milli>
  (diff).count()/niters;
  std::cout << std::endl << "batched_compute_gram_matrix avg elapsed time: "
  << avg_elapsed_time << " ms." << std::endl;

  double* full_gram_matrix = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*s*batch_size*s*batch_size));
  for (unsigned int i = 0; i < niters; ++i) {
    start = std::chrono::steady_clock::now();
    sampled_csr_gemm(s*batch_size, blocked_gramidxs, s*batch_size
    , blocked_gramidxs, 1., rowptr, colptr, data, full_gram_matrix);
    end = std::chrono::steady_clock::now();
    diff += end - start;
  }

  for (unsigned int i = 0; i < s*batch_size; ++i) {
    for (unsigned int j = 0; j < i; ++j) {
      std::cout << std::fixed << std::setprecision(8)
      << full_gram_matrix[i*s*batch_size + j] << ' ';
    }
    std::cout << std::endl;
  }

  std::cout << std::endl;
  avg_elapsed_time = std::chrono::duration<double, std::milli>
  (diff).count()/niters;
  std::cout << std::endl << "sampled_csr_gemm avg elapsed time: "
  << avg_elapsed_time << " ms." << std::endl;

  ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr), ALIGNED_FREE(data);
  ALIGNED_FREE(gramidxs), ALIGNED_FREE(blocked_gramidxs);
  ALIGNED_FREE(gram_matrix), ALIGNED_FREE(blocked_gram_matrix);
  MPI_Finalize();
  return 0;
}
