// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <mpi.h>

#include <iostream>
#include <string>

#include "utils.hpp"
#include "io_utils_mpi.hpp"
#include "generate_data.hpp"
#include "logistic_regression_mpi.hpp"

int main(int argc, char* argv[]) {
  MPI_Init(&argc, &argv);
  int mpi_rank, mpi_size;
  MPI_Comm comm;
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
  MPI_Comm_dup(MPI_COMM_WORLD, &comm);
  int nrows = 4000;
  int ncols_per_rank = 10000;
  double density = 0.0013;
  int nnz_per_rank = static_cast<int>(nrows*ncols_per_rank*density);

  int* rowptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*(nrows+1)));
  int* colptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*(nnz_per_rank)));
  double* data = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*(nnz_per_rank)));
  double* labels = static_cast<double*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(double)*(nrows)));

  random_csr_per_rank<double>
  (comm, nrows, ncols_per_rank, density, rowptr, colptr, data, labels);

  ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr), ALIGNED_FREE(data);
  ALIGNED_FREE(labels);
  MPI_Finalize();
  return 0;
}
