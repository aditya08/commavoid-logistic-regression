// Copyright 2019 Aditya Devarakonda.
#include <mpi.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <string>

#include "utils.hpp"
#include "io_utils_mpi.hpp"

int main(int argc, char** argv) {
  if (argc < 2) {
      std::cout << "Not enough arguments." << std::endl;
      std::cout << "Usage: " << argv[0] << "[Filename]" << std::endl;
      return -1;
  }

  MPI_Init(&argc, &argv);
  int mpi_rank;
  int mpi_size;
  MPI_Comm comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &comm);
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);
  int* rowptr = nullptr;
  int* colptr = nullptr;
  double* data = nullptr;
  double* labels = nullptr;
  std::cout << "Calling parallel_read" << std::endl;
  Read_Statistics rd_stats = parallel_read(comm, argv[1],
  &rowptr, &colptr, &data, &labels);
  int print_flag;
  if (mpi_rank == 0) {
    std::cout << "Total matrix size is " << rd_stats.nrows_total << " rows by "
    << rd_stats.ncols_total << " columns." << std::endl;
    std::cout << "nnz = " << rd_stats.nnz_total << " density = "
    << static_cast<double>(rd_stats.nnz_total)/
    rd_stats.nrows_total/rd_stats.ncols_total << std::endl << std::endl;

    std::cout << "[Rank " << mpi_rank << "] matrix size is " << rd_stats.nrows
    << " rows by " << rd_stats.ncols << " columns."
    << " nnz = " << rd_stats.nnz << std::endl;
    if (mpi_size > 1) {
      MPI_Send(&rd_stats.ncols, 1, MPI_INT, 1, 0, comm);
    }
  } else if (mpi_rank > 0 && mpi_rank < mpi_size - 1) {
    MPI_Recv(&print_flag, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG,
    comm, MPI_STATUS_IGNORE);
    print_flag+=rd_stats.ncols;
    std::cout << "[Rank " << mpi_rank << "] matrix size is " << rd_stats.nrows
    << " rows by " << rd_stats.ncols << " columns."
    << " nnz = " << rd_stats.nnz << std::endl;
    MPI_Send(&print_flag, 1, MPI_INT, mpi_rank+1, 0, comm);
  } else {
    MPI_Recv(&print_flag, 1, MPI_INT, mpi_rank - 1, MPI_ANY_TAG,
    comm, MPI_STATUS_IGNORE);
    print_flag+=rd_stats.ncols;
    std::cout << "[Rank " << mpi_rank << "] matrix size is " << rd_stats.nrows
    << " rows by " << rd_stats.ncols << " columns."
    << " nnz = " << rd_stats.nnz << std::endl;
    std::cout << "Total ncols: " << print_flag << std::endl;
  }
  // const std::string out_fname = "mushrooms_rank" + std::to_string(mpi_rank);
  // parallel_write(comm, out_fname.c_str(), rd_stats,
  // rowptr, colptr, data, labels);
  ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr);
  ALIGNED_FREE(data), ALIGNED_FREE(labels);
  MPI_Finalize();

  return 0;
}
