// Copyright 2019 Aditya Devarakonda.
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

#include "utils.hpp"
#include "io_utils.hpp"

int main(int argc, char** argv) {
    if (argc < 2) {
        std::cout << "No enough arguments." << std::endl;
        std::cout << "Usage: " << argv[0] << "[Filename]" << std::endl;
    }

    int* rowptr = 0;
    int* colptr = 0;
    double* data = 0;
    double* labels = 0;

    Read_Statistics rd_stats = read(argv[1], &rowptr, &colptr, &data, &labels);

    std::cout << "Matrix size is " << rd_stats.nrows << " rows by "
    << rd_stats.ncols << " columns." << std::endl;
    std::cout << "nnz = " << rd_stats.nnz << " density = "
    <<static_cast<double>(rd_stats.nnz)/rd_stats.nrows/rd_stats.ncols
    << std::endl;
    // write("mushrooms", rd_stats, rowptr, colptr, data, labels);
    ALIGNED_FREE(rowptr), ALIGNED_FREE(colptr);
    ALIGNED_FREE(data), ALIGNED_FREE(labels);
    return 0;
}
