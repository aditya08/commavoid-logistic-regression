// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <mpi.h>
#include <iostream>
#include <random>

#include "generate_data.hpp"

template <typename T>
void random_csr_per_rank(MPI_Comm comm, int m, int n, T density,
int* rowptr, int *colptr, T* data, T* labels, bool rows_per_rank) {
  int mpi_rank;
  int mpi_size;
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);

  int nrows, ncols;
  if (rows_per_rank) {
    // 1D-row layout. Each rank has m rows.
    nrows = m*mpi_size;
    ncols = n;
  } else {
    // 1D-col layout. Each rank has m cols.
    nrows = m;
    ncols = n*mpi_size;
  }
  // compute nnz per rank.
  int nnz_total = static_cast<int>(nrows*ncols*density);
  int nnz_per_rank = nnz_total/mpi_size;
  if (mpi_rank < (nnz_total % mpi_size)) {
    nnz_per_rank++;
  }
  // set rng
  std::random_device rand_dev;
  std::mt19937_64 rand_gen(rand_dev());
  std::bernoulli_distribution label_dist(0.5);
  int count_total = 0;
  int count_per_row = 0;
  int nnz_per_row = nnz_per_rank/m;
  std::cout << "nnz_per_row: "  << nnz_per_row << std::endl;
  // generate random data.
  rowptr[0] = 0;
  for (int i = 0; i < m; ++i) {
    count_per_row = 0;
    nnz_per_row = (nnz_per_rank - count_total) / (m-i);
    if (label_dist(rand_gen)) {
      labels[i] = 1.;
    } else {
      labels[i] = -1.;
    }
    for (int j = 0; j < n; ++j) {
      std::bernoulli_distribution dist((nnz_per_row - count_per_row)/(n-j));
      if (dist(rand_gen) && count_per_row < nnz_per_row) {
        colptr[count_total] = j;
        data[count_total] = static_cast<T>(std::rand())/RAND_MAX;
        ++count_total;
        ++count_per_row;
      }
    }
    rowptr[i+1] = count_per_row + rowptr[i];
  }
  std::cout << "Expected count: " << nnz_per_rank << std::endl;
  std::cout << "Actual count: " << count_total << std::endl;
}

template void random_csr_per_rank<float>(MPI_Comm, int, int, float, int*, \
int*, float*, float*, bool);
template void random_csr_per_rank<double>(MPI_Comm, int, int, double, int*, \
int*, double*, double*, bool);
