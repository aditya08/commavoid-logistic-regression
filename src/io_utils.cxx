// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <cstring>

#include "utils.hpp"
#include "io_utils.hpp"

/*
    Read LIBSVM-format binary classification data.
    This method might perform ok for small files. However, for large files a chunked approach is better.
 */
template <typename T>
Read_Statistics read(const char* filename,
int** out_rowptr, int** out_colptr, T** out_data, T** out_labels) {
  Read_Statistics rd_stats;
  rd_stats.nrows = rd_stats.ncols = rd_stats.nnz = 0;
  if (filename == nullptr) {
    std::cerr << "[read]: filename is nullptr." << std::endl;
    return rd_stats;
  }

  std::vector<int> rowptr, colptr;
  std::vector<T> data, labels;

  std::ifstream datafile(filename);
  std::string line;
  std::string buf = "";
  int idx = 0;
  int nnz_row = 0;
  double val = 0.;
  int ncol_max  = -1;
  bool one_two_labels = false;
  rowptr.push_back(0);

  while (std::getline(datafile, line)) {
      std::istringstream str_stream(line);
      buf = "";
      while (str_stream >> buf) {
          size_t colon_pos = buf.find(":");
          if (colon_pos == std::string::npos) {  // this is a label.
              if (buf.find('+') != std::string::npos) {
                  labels.push_back(1.);
              } else if (buf.find('-') != std::string::npos) {
                  labels.push_back(-1.);
              } else if (buf.find('2') != std::string::npos) {
                  if (!one_two_labels) {
                    // change '1' labels to '-1'
                    // because we just found a label called '2'.
                    for (unsigned int i = 0; i < labels.size(); ++i) {
                      labels[i] = (labels[i] == static_cast<T>(1.))
                      ? static_cast<T>(-1.) : labels[i];
                    }
                    one_two_labels = true;
                  }
                  labels.push_back(1.);
              } else if (buf.find('1') != std::string::npos) {
                  if (one_two_labels) {
                    labels.push_back(-1.);
                  } else {
                    labels.push_back(1.);
                  }
              } else if (buf.find('0') != std::string::npos) {
                    labels.push_back(-1.);
              } else {
                  std::cout << buf << std::endl;
                  std::cerr << "Unhandled character found." << std::endl;
                  return rd_stats;
              }
          } else {
             idx = std::stoi(buf.substr(0, colon_pos));
             val = std::stod(buf.substr(colon_pos+1, buf.length()));

              ncol_max = (idx > ncol_max) ? idx : ncol_max;
              nnz_row++;
              colptr.push_back(idx-1);
              data.push_back(static_cast<T>(val));
          }
      }
      rowptr.push_back(nnz_row);
  }
  // return read stats to caller.
  rd_stats.ncols = ncol_max;
  rd_stats.nrows = static_cast<unsigned int>(rowptr.size()-1);
  rd_stats.nnz = static_cast<unsigned int>(data.size());
  // move content from std::vector to byte-aligned array.
  *out_rowptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*rowptr.size()));
  if (*out_rowptr == nullptr) {
      std::cerr << "[read]: out_rowptr is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_rowptr, rowptr.data(), sizeof(int)*rowptr.size());
  rowptr.clear();
  *out_colptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*colptr.size()));
  if (*out_colptr == nullptr) {
      std::cerr << "[read]: out_colptr is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_colptr, colptr.data(), sizeof(int)*colptr.size());
  colptr.clear();
  *out_data = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*data.size()));
  if (*out_data == nullptr) {
      std::cerr << "[read]: out_data is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_data, data.data(), sizeof(T)*data.size());
  data.clear();
  *out_labels = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*labels.size()));
  if (*out_labels == nullptr) {
      std::cerr << "[read]: out_labels is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_labels, labels.data(), sizeof(T)*labels.size());
  labels.clear();
  return rd_stats;
}

/*
    A chunked reader. This should be faster than read() for large files.
 */
int chunked_read(char* filename, std::vector<int>* rowptr,
std::vector<int>* colptr, std::vector<double>* data,
std::vector<double>* labels) {
    // TODO(Aditya): Finish implementation.
    std::cerr << "[chunked_read] Method not implemented." << std::endl;
    return 0;
}

/*
    A multi-threaded reader.
 */
int threaded_read(char* filename, std::vector<int>* rowptr,
std::vector<int>* colptr, std::vector<double>* data,
std::vector<double>* labels) {
    // TODO(Aditya): Finish implementation.
    std::cerr << "[threaded_read] Method not implemented." << std::endl;
    return 0;
}


/*
    A multi-threaded and chunked reader.
 */
int threaded_chunked_read(char* filename, std::vector<int>* rowptr,
std::vector<int>* colptr, std::vector<double>* data,
std::vector<double>* labels) {
    // TODO(Aditya): Finish implementation.
    std::cerr << "[threaded_chunked_read] Method not implemented." << std::endl;
    return 0;
}

template <typename T>
void write(const char* filename, Read_Statistics rd_stats,
int* rowptr, int* colptr, T* data, T* labels) {
  if (filename == nullptr || rowptr == nullptr || colptr == nullptr
  || data == nullptr || labels == nullptr) {
    std::cerr << "[write]: pointers are nullptrs." << std::endl;
    return;
  }
  if (rd_stats.nrows <= 0 || rd_stats.ncols <= 0 || rd_stats.nnz <= 0) {
    std::cerr << "[write]: rd_stats settings are invalid." << std::endl;
    return;
  }
  std::ofstream file(filename, std::ofstream::out);
  std::string buf = "";
  for (unsigned int i = 1; i < rd_stats.nrows+1; ++i) {
      if (labels[i-1] == -1.) {
          buf += "1 ";
      } else if (labels[i-1] == 1.) {
          buf += "2 ";
      }
      for (int j = rowptr[i-1]; j < rowptr[i]; ++j) {
          buf += std::to_string(colptr[j]+1) + ":1 ";
      }
      buf += "\n";
  }
  file << buf;
  file.close();
}

template Read_Statistics read<double>
(const char*, int**, int**, double**, double**);
template Read_Statistics read<float>
(const char*, int**, int**, float**, float**);
template void write<double>
(const char*, Read_Statistics, int*, int*, double*, double*);
template void write<float>
(const char*, Read_Statistics, int*, int*, float*, float*);
