// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include "logistic_regression_cpu.hpp"

#include <omp.h>
#include <immintrin.h>

#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <chrono>

#include "utils.hpp"

template <typename T>
T training_loss(unsigned int nrows,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
  if (rowptr == nullptr || colptr == nullptr || data == nullptr
  || labels == nullptr || weights == nullptr) {
    std::cerr <<
    "[training_loss]: pointers are nullptrs."
    << std::endl;
    return -1.;
  }
  if (nrows <= 0) {
    std::cerr << "[training_loss]: nrows settings are incorrect." << std::endl;
    return -1.;
  }
  T loss = 0.;
  T* loss_vec = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*nrows));
  if (loss_vec == nullptr) {
    std::cerr << "[training_loss]: loss_vec is a nullptr." << std::endl;
    return -1.;
  }
  std::memset(loss_vec, 0, sizeof(T)*nrows);
  T alpha = 1.;
  gemv(nrows, alpha, rowptr, colptr, data, weights, loss_vec);
  for (unsigned int i = 0; i < nrows; ++i) {
      loss += log(static_cast<T>(1.)
      + exp(static_cast<T>(-1.)*labels[i]*loss_vec[i]));
  }
  free(loss_vec);
  return loss/nrows;
}

template <typename T>
T training_accuracy(unsigned int nrows,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
  if (rowptr == nullptr || colptr == nullptr || data == nullptr
  || labels == nullptr || weights == nullptr) {
    std::cerr <<
    "[training_accuracy]: pointers are nullptrs."
    << std::endl;
    return -1.;
  }
  if (nrows <= 0) {
    std::cerr << "[training_accuracy]: nrows settings are incorrect."
    << std::endl;
    return -1.;
  }
  T accuracy;
  T* probabilities = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*nrows));
  if (probabilities == nullptr) {
    std::cerr << "[training_accuracy]: probabilities is a nullptr."
    << std::endl;
    return -1.;
  }
  std::memset(probabilities, 0, sizeof(T)*nrows);
  T alpha = -1.;
  T ncorrect = 0.;
  gemv(nrows, alpha, rowptr, colptr, data, weights, probabilities);
  for (unsigned int i = 0; i < nrows; ++i) {
      probabilities[i] = static_cast<T>(1.)/(static_cast<T>(1.)
      + exp(probabilities[i]));
      if (probabilities[i] > .5 && labels[i] == 1.) {
          ncorrect++;
      } else if (probabilities[i] <= .5 && labels[i] == -1.) {
          ncorrect++;
      }
  }
  accuracy = ncorrect/nrows;
  free(probabilities);
  return static_cast<T>(100.)*accuracy;
}

template <typename T>
void logistic_regression(Options<T> opt,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
  if (rowptr == nullptr || colptr == nullptr || data == nullptr
  || labels == nullptr || weights == nullptr) {
    std::cerr <<
    "[logistic_regression]: pointers are nullptrs."
    << std::endl;
    return;
  }
  if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
  || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
  || opt.log_freq <= 0 || opt.log_freq > opt.max_iters || opt.batch_size <= 0
  || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
    std::cerr <<
    "[logistic_regression]: Options settings are incorrect."
    << std::endl;
    return;
  }
  std::srand(opt.seed);
  unsigned int iter_count = 0;
  std::memset(weights, 0, sizeof(T)*opt.ncols);
  T residual = -1.;
  int sampled_row = -1;
  T* grad = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.ncols));
  if (grad == nullptr) {
    std::cerr << "[logistic_regression]: grad is a nullptr." << std::endl;
    return;
  }
  std::memset(grad, 0, sizeof(T)*opt.ncols);
  T loss = -1.;
  T accuracy = 0.;
  T eta_div_nrows = opt.eta/opt.nrows;
  while (iter_count < opt.max_iters) {
    // pseudo-uniform random sampling.
    // sampled_row = ((static_cast<T>(std::rand())/ RAND_MAX) * opt.nrows);
    // cyclic sampling.
    sampled_row = iter_count % opt.nrows;
    // compute the residual for row.
    sparse_dot(sampled_row, rowptr, colptr, data, weights, &residual);
    residual = static_cast<T>(1.)/(static_cast<T>(1.)
    + exp(labels[sampled_row]*residual));
    // compute the gradient.
    sparse_scal(sampled_row, labels[sampled_row]*residual,
    rowptr, colptr, data, grad);
    // update the weights.
    axpy(opt.ncols, eta_div_nrows, grad, weights);
    if (iter_count % opt.log_freq == opt.log_freq - 1) {
        loss = training_loss(opt.nrows,
        rowptr, colptr, data, labels, weights);
        accuracy = training_accuracy(opt.nrows,
        rowptr, colptr, data, labels, weights);
        if (loss < 0. || accuracy < 0.) {
          std::cerr << "[logistic_regression]: loss/accuracy is negative."
          << std::endl;
          return;
        }
        std::cout << "Epoch: " << (iter_count+1)/opt.nrows
        << ", Iteration: " << iter_count+1 << ", Loss: "
        << std::setprecision(16) << loss << ", Training Accuracy: "
        << std::setprecision(6) << accuracy << "%" << std::endl;
    }
    // reset residual, grad for next iteration.
    std::memset(grad, 0, sizeof(T)*opt.ncols);
    residual = 0.;
    iter_count++;
  }
  free(grad);
}

template <typename T>
void ca_logistic_regression(Options<T> opt,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
  if (rowptr == nullptr || colptr == nullptr || data == nullptr
  || labels == nullptr || weights == nullptr) {
    std::cerr <<
    "[CA_logistic_regression]: pointers are nullptrs."
    << std::endl;
    return;
  }
  if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
  || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
  || opt.log_freq <= 0 || opt.log_freq >= opt.max_iters || opt.batch_size <= 0
  || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
    std::cerr <<
    "[CA_logistic_regression]: Options settings are incorrect."
    << std::endl;
    return;
  }
  std::srand(opt.seed);
  std::memset(weights, 0, sizeof(T)*opt.ncols);
  unsigned int iter_count = 0;
  unsigned int gram_nvals = ((opt.s-1)*opt.s)/2;
  T* gram_matrix = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*gram_nvals));
  T* residuals = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.s));
  T* grad = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.ncols));
  if (gram_matrix == nullptr || residuals == nullptr || grad == nullptr) {
    std::cerr << "[CA_logistic_regression]: gram_matrix is a nullptr."
    << std::endl;
    return;
  }
  std::memset(gram_matrix, 0, sizeof(T)*gram_nvals);
  std::memset(residuals, 0, sizeof(T)*opt.s);
  T residual_correction = 0.;
  std::memset(grad, 0, sizeof(T)*opt.ncols);
  T loss = -1.;
  T accuracy = 0.;
  const T scale_factor_one = 1.;
  T eta_div_nrows = opt.eta/opt.nrows;
  int* sampled_rows = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*opt.s));
  T* sampled_labels = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.s));

  while (iter_count < opt.max_iters) {
    // sample s rows.
    for (unsigned int i = 0; i < opt.s; ++i) {
      // pseudo-uniform random sampling.
      // sampled_rows[i] = (int)((static_cast<T>
      // (std::rand())/ RAND_MAX) * opt.nrows);
      // cyclic sampling.
      sampled_rows[i] = (iter_count+i) % opt.nrows;
      sampled_labels[i] = labels[sampled_rows[i]];
    }
    // compute s residuals.
    sparse_gemv(opt.s, sampled_rows, scale_factor_one,
    rowptr, colptr, data, weights, residuals);
    compute_gram_matrix(opt.s, sampled_rows, scale_factor_one,
    rowptr, colptr, data, gram_matrix);

    // perform s-step updates.
    unsigned int offset = 0;
    for (unsigned int i = 0; i < opt.s; ++i) {
      offset = (i*(i-1))/2;
      // correct the i-th residual based on previous solutions.
      for (unsigned int j = 0; j < i; ++j) {
          // if batch_size > 1, need to use gemv for gram_matrix*residuals.
          residual_correction += eta_div_nrows*(sampled_labels[i]
          *sampled_labels[j]*gram_matrix[offset + j]*residuals[j]);
      }
      residuals[i] = static_cast<T>(1.)/(static_cast<T>(1.)
      + exp(sampled_labels[i]*residuals[i] + residual_correction));
      sparse_scal(sampled_rows[i], sampled_labels[i]*residuals[i],
      rowptr, colptr, data, grad);
      // update the weights.
      axpy(opt.ncols, eta_div_nrows, grad, weights);
      if (iter_count % opt.log_freq == opt.log_freq - 1) {
          loss = training_loss(opt.nrows,
          rowptr, colptr, data, labels, weights);
          accuracy = training_accuracy(opt.nrows,
          rowptr, colptr, data, labels, weights);
          if (loss < 0. || accuracy < 0.) {
            std::cerr << "[CA_logistic_regression]: loss/accuracy is negative."
            << std::endl;
            return;
          }
          std::cout << "Epoch: " << (iter_count+1)/opt.nrows
          << ", Iteration: " << iter_count+1 << ", Loss: "
          << std::setprecision(16) << loss << ", Training Accuracy: "
          << std::setprecision(6) << accuracy << "%" << std::endl;
      }
      iter_count++;
      if (iter_count == opt.max_iters) {
        free(gram_matrix), free(residuals), free(grad);
        return;
      }
      residual_correction = 0.;
      std::memset(grad, 0, sizeof(T)*opt.ncols);
    }
    // reset residuals, grad, residual_correction for next iteration.
    residual_correction = 0;
    std::memset(residuals, 0, sizeof(T)*opt.s);
    std::memset(gram_matrix, 0, sizeof(T)*gram_nvals);
  }
  free(gram_matrix), free(residuals), free(grad);
}

template <typename T>
void sparse_axpy(int sampled_rowidx, T alpha,
int* rowptr, int* colptr, T* data, T* out_vec) {
  if (sampled_rowidx < 0 || rowptr == nullptr || colptr == nullptr
  || data == nullptr || out_vec == nullptr) {
    std::cerr <<
    "[sparse_axpy]: sampled_rowidx < 0 or pointers are nullptrs."
    << std::endl;
    return;
  }
  int row_startidx = rowptr[sampled_rowidx];
  int row_endidx = rowptr[sampled_rowidx+1];
  for (int i = row_startidx; i < row_endidx; ++i) {
    out_vec[colptr[i]] += alpha*data[i];
  }
}

template <typename T>
void axpy(unsigned int out_vec_len, T alpha, T* in_vec, T* out_vec) {
  if (out_vec_len <= 0 || in_vec == nullptr || out_vec == nullptr) {
    std::cerr <<
    "[axpy]: out_vec_len < 0 or pointers are nullptrs."
    << std::endl;
    return;
  }
  for (unsigned int i = 0; i < out_vec_len; ++i) {
    out_vec[i] += alpha*in_vec[i];
  }
}

template <typename T>
void sparse_scal(int sampled_rowidx, T alpha,
int* rowptr, int* colptr, T* data, T* out_vec) {
  if (sampled_rowidx < 0 || rowptr == nullptr || colptr == nullptr
  || data == nullptr || out_vec == nullptr) {
    std::cerr <<
    "[sparse_scal]: sampled_rowidx < 0 or pointers are nullptrs."
    << std::endl;
    return;
  }
  int row_startidx = rowptr[sampled_rowidx];
  int row_endidx = rowptr[sampled_rowidx+1];
  for (int i = row_startidx; i < row_endidx; ++i) {
    out_vec[colptr[i]] = alpha*data[i];
  }
}

template <typename T>
void scal(unsigned int out_vec_len, T alpha, T* out_vec) {
  if (out_vec_len <= 0 || out_vec == nullptr) {
    std::cerr <<
    "[scal]: out_vec_len <= 0 or out_vec is nullptr."
    << std::endl;
    return;
  }
  for (unsigned int i = 0; i < out_vec_len; ++i) {
    out_vec[i] *= alpha;
  }
}
template <typename T>
void sparse_dot(int sampled_rowidx,
int* rowptr, int* colptr, T* data, T* in_vec, T* out_scalar) {
  if (sampled_rowidx < 0 || rowptr == nullptr || colptr == nullptr
  || data == nullptr || in_vec == nullptr || out_scalar == nullptr ) {
    std::cerr <<
    "[sparse_dot]: sampled_rowidx < 0 or pointers are nullptrs."
    << std::endl;
    return;
  }
  *out_scalar = 0.;
  int row_startidx = rowptr[sampled_rowidx];
  int row_endidx = rowptr[sampled_rowidx+1];
  for (int i = row_startidx; i < row_endidx; ++i) {
    *out_scalar += data[i]*in_vec[colptr[i]];
  }
}

template <typename T>
void dot(unsigned int nvals, T* row_vec, T* col_vec, T* dotprod) {
  if (nvals <= 0 || row_vec == nullptr || col_vec == nullptr
  || dotprod == nullptr) {
    std::cerr <<
    "[dot]: nvals <= 0 or pointers are nullptrs."
    << std::endl;
    return;
  }
  *dotprod = 0.;
  for (unsigned int i = 0; i < nvals; ++i) {
    *dotprod += row_vec[i]*col_vec[i];
  }
}

template <typename T>
void dot_unrolled(unsigned int nvals, T* row_vec, T* col_vec, T* dotprod) {
  if (nvals <= 0 || row_vec == nullptr || col_vec == nullptr
  || dotprod == nullptr) {
    std::cerr <<
    "[dot_unrolled]: nvals <= 0 or pointers are nullptrs." << std::endl;
    return;
  }
  const unsigned int LOOP_TILE_SIZE = 10;
  unsigned int remainder = nvals % LOOP_TILE_SIZE;
  *dotprod = 0.;
  for (unsigned int i = 0; i < remainder; ++i) {
    *dotprod += row_vec[i]*col_vec[i];
  }
  for (unsigned int i = remainder; i < nvals; i+=LOOP_TILE_SIZE) {
    *dotprod += row_vec[i]*col_vec[i] +
    row_vec[i+1]*col_vec[i+1] +
    row_vec[i+2]*col_vec[i+2] +
    row_vec[i+3]*col_vec[i+3] +
    row_vec[i+4]*col_vec[i+4] +
    row_vec[i+5]*col_vec[i+5] +
    row_vec[i+6]*col_vec[i+6] +
    row_vec[i+7]*col_vec[i+7] +
    row_vec[i+8]*col_vec[i+8] +
    row_vec[i+9]*col_vec[i+9];
  }
}

template <typename T>
void gemv(unsigned int nrows, T alpha,
int* rowptr, int* colptr, T* data, T* in_vec, T* out_vec) {
  if (nrows <= 0 || rowptr == nullptr || colptr == nullptr || data == nullptr
  || in_vec == nullptr || out_vec == nullptr) {
    std::cerr <<
    "[gemv]: nrows <= 0 or pointers are nullptrs." << std::endl;
    return;
  }
  for (unsigned int i = 0; i < nrows; ++i) {
    for (int j = rowptr[i]; j < rowptr[i+1]; ++j) {
      out_vec[i] += data[j]*in_vec[colptr[j]];
    }
    out_vec[i] *= alpha;
  }
}

template <typename T>
void sparse_gemv(unsigned int s, int* sampled_rowidxs, T alpha,
int* rowptr, int* colptr, T* data, T* in_vec, T* out_vec) {
  if (s < 1 || sampled_rowidxs == nullptr || rowptr == nullptr
  || colptr == nullptr || data == nullptr || in_vec == nullptr
  || out_vec == nullptr) {
    std::cerr <<
    "[sparse_gemv]: s < 1 or pointers are nullptrs." << std::endl;
    return;
  }
  int row_startidx = -1;
  int row_endidx = -1;
  T sum = 0.;
  #pragma omp parallel for schedule(dynamic)
  for (unsigned int i = 0; i < s; ++i) {
    row_startidx = rowptr[sampled_rowidxs[i]];
    row_endidx = rowptr[sampled_rowidxs[i]+1];
    for (int j = row_startidx; j < row_endidx; ++j) {
      sum += data[j]*in_vec[colptr[j]];
    }
    out_vec[i] = sum*alpha;
    sum = 0.;
  }
}

template <typename T>
void compute_gram_matrix(unsigned int s, int* gramidxs, T alpha,
int* rowptr, int* colptr, T* data,  T* dense_out_mat) {
  if (s < 1 || gramidxs == nullptr || rowptr == nullptr || colptr == nullptr
  || data == nullptr || dense_out_mat == nullptr) {
    std::cerr <<
    "[compute_gram_matrix]: s < 1 or pointers are nullptrs." << std::endl;
    return;
  }
  int row_startidx = -1;
  int row_endidx = -1;
  int col_startidx = -1;
  int col_endidx = -1;
  unsigned int offset = 0;
  T sum = 0.;
  #pragma omp parallel for schedule(dynamic)
  for (unsigned int i = 0; i < s; ++i) {
    row_startidx = rowptr[gramidxs[i]];
    row_endidx = rowptr[gramidxs[i]+1];
    for (unsigned int j = i+1; j < s; ++j) {
      offset = (j*(j-1))/2;
      col_startidx = rowptr[gramidxs[j]];
      col_endidx = rowptr[gramidxs[j]+1];
      for (int k = row_startidx; k < row_endidx; ++k) {
        // assumes sorted column indexes.
        for (int l = col_startidx; l < col_endidx; ++l) {
          if (colptr[k] < colptr[l]) {
            break;
          } else if (colptr[k] == colptr[l]) {
            sum += data[k]*data[l];
            // advance col_startidx to reduce idx search since colptr is sorted.
            col_startidx = l;
            break;
          }
        }
      }
      dense_out_mat[offset + i] = alpha*sum;
      sum = 0.;
    }
  }
}

// template instantiations.
// TODO(Aditya): reduced-precision instantiations (when supported).
template float training_loss<float>
(unsigned int, int*, int*, float*, float*, float*);
template double training_loss<double>
(unsigned int, int*, int*, double*, double*, double*);
template float training_accuracy<float>
(unsigned int, int*, int*, float*, float*, float*);
template double training_accuracy<double>
(unsigned int, int*, int*, double*, double*, double*);
template void logistic_regression<float>
(Options<float>, int*, int*, float*, float*, float*);
template void logistic_regression<double>
(Options<double>, int*, int*, double*, double*, double*);
template void ca_logistic_regression<float>
(Options<float>, int*, int*, float*, float*, float*);
template void ca_logistic_regression<double>
(Options<double>, int*, int*, double*, double*, double*);
template void sparse_axpy<float>(int, float, int*, int*, float*, float*);
template void sparse_axpy<double>(int, double, int*, int*, double*, double*);
template void sparse_scal<float>(int, float, int*, int*, float*, float*);
template void sparse_scal<double>(int, double, int*, int*, double*, double*);
template void sparse_dot<float>(int, int*, int*, float*, float*, float*);
template void sparse_dot<double>(int, int*, int*, double*, double*, double*);
template void sparse_gemv<float>
(unsigned int, int*, float, int*, int*, float*, float*, float*);
template void sparse_gemv<double>
(unsigned int, int*, double, int*, int*, double*, double*, double*);
template void axpy<float>(unsigned int, float, float*, float*);
template void axpy<double>(unsigned int, double, double*, double*);
template void scal<float>(unsigned int, float, float*);
template void scal<double>(unsigned int, double, double*);
template void dot<float>(unsigned int, float*, float*, float*);
template void dot<double>(unsigned int, double*, double*, double*);
template void dot_unrolled<float>(unsigned int, float*, float*, float*);
template void dot_unrolled<double>(unsigned int, double*, double*, double*);
template void gemv<float>
(unsigned int, float, int*, int*, float*, float*, float*);
template void gemv<double>
(unsigned int, double, int*, int*, double*, double*, double*);
template void compute_gram_matrix<float>
(unsigned int, int*, float, int*, int*, float*, float*);
template void compute_gram_matrix<double>
(unsigned int, int*, double, int*, int*, double*, double*);
