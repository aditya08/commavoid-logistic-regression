// Copyright 2019 - 2022 Aditya Devarakonda. All rights reserved.
#include <iostream>
#include <iomanip>

#include <mpi.h>
#include <mkl.h>
#include <mkl_spblas.h>

#include "utils.hpp"
#include "logistic_regression_mkl_mpi.hpp"

#ifdef __linux__
    #include <cstdlib>
    #include <cstring>
    #include <cmath>
    #include <algorithm>
#endif

template <typename T>
T training_loss(MPI_Comm comm, unsigned int nrows,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
    if (nrows <= 0 || rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr << "[training_loss]: nrows <= 0 or pointers are nullptrs."
        << std::endl;
        return -1.;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    T loss = 0.;
    T* loss_vec = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*nrows));
    T* loss_vec_reduced = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*nrows));
    if (loss_vec == nullptr || loss_vec_reduced == nullptr) {
        std::cerr <<
        "[training_loss]: loss_vec or loss_vec_reduced are nullptrs."
        << std::endl;
        return -1.;
    }
    std::memset(loss_vec, 0, sizeof(T)*nrows);
    std::memset(loss_vec_reduced, 0, sizeof(T)*nrows);
    T alpha = 1.;
    gemv(nrows, alpha, rowptr, colptr, data, weights, loss_vec);
    MPI_Allreduce(loss_vec, loss_vec_reduced, nrows,
    get_MPI_Datatype(loss_vec), MPI_SUM, comm);
    for (unsigned int i = 0; i < nrows; ++i) {
        loss += log(static_cast<T>(1.) +
        exp(static_cast<T>(-1.)*labels[i]*loss_vec_reduced[i]));
    }
    ALIGNED_FREE(loss_vec), ALIGNED_FREE(loss_vec_reduced);
    return loss/nrows;
}

template <typename T>
T training_accuracy(MPI_Comm comm, unsigned int nrows,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
    if (nrows <= 0 || rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr << "[training_accuracy]: nrows <= 0 or pointers are nullptrs."
        << std::endl;
        return -1.;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    T accuracy;
    T* probabilities = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*nrows));
    T* probabilities_reduced = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*nrows));
    if (probabilities == nullptr || probabilities_reduced == nullptr) {
        std::cerr <<
        "[training_accuracy]: probabilities/probabilities_reduced are nullptrs."
        << std::endl;
        return -1.;
    }
    std::memset(probabilities, 0, sizeof(T)*nrows);
    std::memset(probabilities_reduced, 0, sizeof(T)*nrows);
    T alpha = -1.;
    T ncorrect = 0.;
    gemv(nrows, alpha, rowptr, colptr, data, weights, probabilities);
    MPI_Allreduce(probabilities, probabilities_reduced, nrows,
    get_MPI_Datatype(probabilities), MPI_SUM, comm);
    for (unsigned int i = 0; i < nrows; ++i) {
        probabilities_reduced[i] = static_cast<T>(1.)/
        (static_cast<T>(1.) + exp(probabilities_reduced[i]));
        if (probabilities_reduced[i] > .5 && labels[i] == 1.) {
                ncorrect++;
        } else if (probabilities_reduced[i] <= .5 && labels[i] == -1.) {
                ncorrect++;
        }
    }
    accuracy = ncorrect/nrows;
    ALIGNED_FREE(probabilities);
    ALIGNED_FREE(probabilities_reduced);
    return static_cast<T>(100.)*accuracy;
}

template <typename T>
void logistic_regression(MPI_Comm comm, Options<T> opt,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
#ifdef TAU_PROFILING
    TAU_PROFILE_TIMER(setuptimer, "Setup Time", "args check and ALIGNED_ALLOCs",
    TAU_DEFAULT);
    TAU_PROFILE_TIMER(misctimer, "Miscellaneous Time",
    "sampling, scalar calcs and grad memset", TAU_DEFAULT);
    TAU_PROFILE_START(setuptimer);
#endif
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[logistic_regression]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[logistic_regression]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    unsigned int iter_count = 0;
    std::memset(weights, 0, sizeof(T)*opt.ncols);
    T residual = -1., residual_reduced = -1;
    int sampled_row = -1;
    T* grad = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.ncols));
    if (grad == nullptr) {
        std::cerr << "[logistic_regression]: grad is a nullptr." << std::endl;
        return;
    }
    std::memset(grad, 0, sizeof(T)*opt.ncols);
    T loss = -1.;
    T accuracy = 0.;
    T eta_div_nrows = opt.eta/opt.nrows;
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(setuptimer);
#endif
    while (iter_count < opt.max_iters) {
        // pseudo-uniform random sampling.
        // TODO(Aditya): Make random sampling a user option.
        // sampled_row = ((static_cast<T>(std::rand())/ RAND_MAX) * opt.nrows);
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
        // cyclic sampling.
        sampled_row = iter_count % opt.nrows;
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
        // compute the residual for row.
        sparse_dot(sampled_row, rowptr, colptr, data, weights, &residual);
        MPI_Allreduce(&residual, &residual_reduced, 1,
        get_MPI_Datatype(&residual), MPI_SUM, comm);
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        residual = residual_reduced;
        residual = static_cast<T>(1.)/
        (static_cast<T>(1.) + exp(labels[sampled_row]*residual));
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
        // compute the gradient.
        sparse_scal(sampled_row, labels[sampled_row]*residual,
        rowptr, colptr, data, grad);
        // update the weights.
        axpy(opt.ncols, eta_div_nrows, grad, weights);
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        if (iter_count % opt.log_freq == opt.log_freq - 1) {
            loss = training_loss(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            accuracy = training_accuracy(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            if (loss < 0. || accuracy < 0.) {
                std::cerr <<
                "[logistic_regression]: loss/accuracy is negative."
                << std::endl;
                return;
            }
            if (mpi_rank == 0) {
                std::cout << "Epoch: " << (iter_count+1)/opt.nrows
                << ", Iteration: " << iter_count+1 << ", Loss: "
                << std::setprecision(16) << loss << ", Training Accuracy: "
                << std::setprecision(6) << accuracy << "%" << std::endl;
            }
        }
        // reset residual, grad for next iteration.
        std::memset(grad, 0, sizeof(T)*opt.ncols);
        residual = 0.;
        iter_count++;
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
    }
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
    ALIGNED_FREE(grad);
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(misctimer);
#endif
}

void logistic_regression_mkl(MPI_Comm comm, Options<float> opt,
int* rowptr, int* colptr, float* data, float* labels, float* weights, int nthreads) {
#ifdef TAU_PROFILING
    TAU_PROFILE_TIMER(setuptimer, "Setup Time", "args check and ALIGNED_ALLOCs",
    TAU_DEFAULT);
    TAU_PROFILE_TIMER(misctimer, "Miscellaneous Time",
    "sampling, scalar calcs and grad memset", TAU_DEFAULT);
    TAU_PROFILE_START(setuptimer);
#endif
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[logistic_regression]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[logistic_regression]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    unsigned int iter_count = 0;
    std::memset(weights, 0, sizeof(float)*opt.ncols);
    float residual = -1., residual_reduced = -1., residual_ref = -1.;
    float scalar_one = 1.;
    int sampled_row = -1;
    int startidx = -1;
    int nnz_row = -1;
    // float* grad_compressed = static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.ncols));
    float* grad = static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.ncols));
    if (grad == nullptr) {
        std::cerr << "[logistic_regression]: grad is a nullptr." << std::endl;
        return;
    }
    std::memset(grad, 0, sizeof(float)*opt.ncols);
    // std::memset(grad_compressed, 0, sizeof(float)*opt.ncols);
    float loss = -1.;
    float accuracy = 0.;
    float eta_div_nrows = opt.eta/opt.nrows;
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(setuptimer);
#endif
    // sparse_matrix_t csrA_sampledrow;
    // sparse_status_t status;
    mkl_set_num_threads(nthreads);
    while (iter_count < opt.max_iters) {
        // pseudo-uniform random sampling.
        // TODO(Aditya): Make random sampling a user option.
        // sampled_row = ((static_cast<float>(std::rand())/ RAND_MAX) * opt.nrows);
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
        // cyclic sampling.
        sampled_row = iter_count % opt.nrows;
        startidx = rowptr[sampled_row];
        nnz_row = rowptr[sampled_row + 1] - rowptr[sampled_row];
        // status = mkl_sparse_d_create_csr(&csrA_sampledrow, SPARSE_INDEX_BASE_ZERO, 1, opt.ncols, rowptr + sampled_row, rowptr + sampled_row + 1, colptr, data);
        // if(status != SPARSE_STATUS_SUCCESS){
        //     std::cerr << "mkl_sparse_d_create_csr did not succeed." << std::endl;
        //     exit(-1);
        // }

#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
        // compute the residual for row.
        // if (get_MPI_Datatype(&residual) == MPI_DOUBLE){
        //     residual_reduced = cblas_ddoti(nnz_row, data + sampled_row, colptr + sampled_row, weights);
        //     MPI_Allreduce(&residual, &residual_reduced, 1,
        //     MPI_DOUBLE, MPI_SUM, comm);
        //     residual = static_cast<float>(1.)/
        //     (static_cast<float>(1.) + exp(labels[sampled_row]*residual_reduced));
        //     cblas_dcopy(nnz_row, data + sampled_row, 1, grad, 1);
        //     cblas_dscal(nnz_row, labels[sampled_row]*residual, grad, 1);
        //     cblas_daxpyi(nnz_row, eta_div_nrows, grad, colptr + sampled_row, weights);
        //     // sparse_scal(sampled_row, labels[sampled_row]*residual,
        //     // rowptr, colptr, data, grad);
        //     // // update the weights.
        //     // axpy(opt.ncols, eta_div_nrows, grad, weights);

        // }
        // else if(get_MPI_Datatype(&residual) == MPI_FLOAT){
            residual = cblas_sdoti(nnz_row, data + startidx, colptr + startidx, weights);
            // sparse_dot(sampled_row, rowptr, colptr, data, weights, &residual_ref);
            // std::cout << "residual_ref: " << residual_ref << std::endl;
            // std::cout << "residual: " << residual << std::endl;
            // std::cout << "sampled_row: " << sampled_row  << " abs(cblas_sdoti - custom sparse_dot):  " << std::abs(residual_ref - residual) << std::endl;
            // if(iter_count == 2){return;}
            MPI_Allreduce(&residual, &residual_reduced, 1,
            MPI_FLOAT, MPI_SUM, comm);
            residual = scalar_one/(scalar_one + exp(labels[sampled_row]*residual_reduced));
            // cblas_scopy(nnz_row, data + startidx, 1, grad, 1);
            // cblas_sscal(nnz_row, labels[sampled_row]*residual, grad, 1);
            cblas_saxpyi(nnz_row, labels[sampled_row]*residual, data + startidx, colptr + startidx, grad);
            cblas_saxpy(opt.ncols, eta_div_nrows, grad, 1, weights, 1);
            // cblas_saxpyi(nnz_row, eta_div_nrows, grad, colptr + startidx, weights);
            // sparse_scal(sampled_row, labels[sampled_row]*residual,
            // rowptr, colptr, data, grad);
            // // update the weights.
            // axpy(opt.ncols, eta_div_nrows, grad, weights);
        // }
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        // residual = residual_reduced;
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        if (iter_count % opt.log_freq == opt.log_freq - 1) {
            loss = training_loss(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            accuracy = training_accuracy(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            if (loss < 0. || accuracy < 0.) {
                std::cerr <<
                "[logistic_regression]: loss/accuracy is negative."
                << std::endl;
                return;
            }
            if (mpi_rank == 0) {
                std::cout << "Epoch: " << (iter_count+1)/opt.nrows
                << ", Iteration: " << iter_count+1 << ", Loss: "
                << std::setprecision(16) << loss << ", Training Accuracy: "
                << std::setprecision(6) << accuracy << "%" << std::endl;
            }
        }
        // reset residual, grad for next iteration.
        std::memset(grad, 0, sizeof(float)*opt.ncols);
        residual = 0.;
        residual_reduced = 0.;
        residual_ref = 0.;
        iter_count++;
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
    }
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
    ALIGNED_FREE(grad);
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(misctimer);
#endif
}


template <typename T>
void mini_batch_logistic_regression(MPI_Comm comm, Options<T> opt,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
#ifdef TAU_PROFILING
    TAU_PROFILE_TIMER(setuptimer, "Setup Time", "args check and ALIGNED_ALLOCs",
    TAU_DEFAULT);
    TAU_PROFILE_TIMER(misctimer, "Miscellaneous Time",
    "sampling, scalar calcs and grad memset", TAU_DEFAULT);
    TAU_PROFILE_START(setuptimer);
#endif
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[mini-batch logistic_regression]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[mini-batch logistic_regression]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    unsigned int iter_count = 0;
    std::memset(weights, 0, sizeof(T)*opt.ncols);
    T* residual =
    static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.batch_size));
    T* residual_reduced =
    static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.batch_size));
    if (mpi_rank == 0) {
        std::cout <<
        "MPI_Allreduce size: " << opt.batch_size*sizeof(T) << " bytes."
        << std::endl;
    }
    int* sampled_row =
    static_cast<int*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*opt.batch_size));
    T* grad = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.ncols));
    if (grad == nullptr) {
        std::cerr <<
        "[mini-batch logistic_regression]: grad is a nullptr."
        << std::endl;
        return;
    }
    std::memset(grad, 0, sizeof(T)*opt.ncols);
    T loss = -1.;
    T accuracy = 0.;
    unsigned int epoch_count = 1;
    unsigned int current_batch_size = opt.batch_size;
    unsigned int last_batch_size = opt.nrows % opt.batch_size;
    // average the mini-batch gradients (divide by opt.batch_size)
    T eta_div_nrows = opt.eta/opt.nrows;
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(setuptimer);
#endif
    const T scale_factor_one = 1.;
    while (iter_count < opt.max_iters) {
        // pseudo-uniform random sampling.
        // TODO(Aditya): make random sampling a user option.
        // sampled_row = ((static_cast<T>(std::rand())/ RAND_MAX) * opt.nrows);
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
        // cyclic sampling.
        current_batch_size = ((iter_count + opt.batch_size - 1) >= epoch_count*opt.nrows) ? last_batch_size : opt.batch_size;
        for (unsigned int i = 0; i < current_batch_size; ++i) {
            sampled_row[i] = (iter_count + i) % opt.nrows;
        }

#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
        // compute the residual for row.
        sparse_gemv(current_batch_size, sampled_row, scale_factor_one,
        rowptr, colptr, data, weights, residual);
        MPI_Allreduce(residual, residual_reduced, current_batch_size,
        get_MPI_Datatype(&residual[0]), MPI_SUM, comm);
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        for (unsigned int i = 0; i < current_batch_size; ++i) {
            residual[i] = static_cast<T>(1.)/
            (static_cast<T>(1.)
            + exp(labels[sampled_row[i]]*residual_reduced[i]));
            residual[i] *= labels[sampled_row[i]];
        }
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
        // compute the gradient.
        sparse_gemv_transposed(current_batch_size, sampled_row, scale_factor_one,
        rowptr, colptr, data, residual, grad);
        // update the weights.
        axpy(opt.ncols, eta_div_nrows, grad, weights);
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        if ((iter_count+current_batch_size) / opt.nrows == epoch_count) {
            epoch_count++;
            loss = training_loss(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            accuracy = training_accuracy(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            if (loss < 0. || accuracy < 0.) {
                std::cerr <<
                "[mini-batch logistic_regression]: loss/accuracy is negative."
                << std::endl;
                return;
            }
            if (mpi_rank == 0) {
                std::cout << "Epoch: " << (iter_count+current_batch_size)/opt.nrows
                << ", Iteration: " << iter_count+current_batch_size << ", Loss: "
                << std::setprecision(16) << loss << ", Training Accuracy: "
                << std::setprecision(6) << accuracy << "%" << std::endl;
            }
        }
        // reset residual, grad for next iteration.
        std::memset(grad, 0, sizeof(T)*opt.ncols);
        std::memset(residual, 0, sizeof(T)*current_batch_size);
        iter_count += current_batch_size;
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
    }
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
    ALIGNED_FREE(grad);
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(misctimer);
#endif
}

void mini_batch_logistic_regression_mkl(MPI_Comm comm, Options<float> opt,
int* rowptr, int* colptr, float* data, float* labels, float* weights, int nthreads) {
#ifdef TAU_PROFILING
    TAU_PROFILE_TIMER(setuptimer, "Setup Time", "args check and ALIGNED_ALLOCs",
    TAU_DEFAULT);
    TAU_PROFILE_TIMER(misctimer, "Miscellaneous Time",
    "sampling, scalar calcs and grad memset", TAU_DEFAULT);
    TAU_PROFILE_START(setuptimer);
#endif
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[mini-batch logistic_regression_mkl]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[mini-batch logistic_regression_mkl]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    unsigned int iter_count = 0;
    std::memset(weights, 0, sizeof(float)*opt.ncols);
    float* residual =
    static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.batch_size));
    float* residual_reduced =
    static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.batch_size));
    // if (mpi_rank == 0) {
    //     std::cout <<
    //     "MPI_Allreduce size: " << opt.batch_size*sizeof(float) << " bytes."
    //     << std::endl;
    // }
    // int* sampled_row =
    // static_cast<int*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*opt.batch_size));
    float* grad = static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.ncols));
    unsigned int last_batch_size = opt.nrows % opt.batch_size;
    unsigned int current_batch_size = opt.batch_size;
    unsigned int startidx = 0;
    sparse_matrix_t csr_sampledRows;
    sparse_status_t spblas_err;
    matrix_descr descr_sampledRows;
    descr_sampledRows.type = SPARSE_MATRIX_TYPE_GENERAL;
    descr_sampledRows.mode = SPARSE_FILL_MODE_FULL;
    descr_sampledRows.diag = SPARSE_DIAG_NON_UNIT;

    if (grad == nullptr) {
        std::cerr <<
        "[mini-batch logistic_regression_mkl]: grad is a nullptr."
        << std::endl;
        return;
    }
    std::memset(grad, 0, sizeof(float)*opt.ncols);
    float loss = -1.;
    float accuracy = 0.;
    unsigned int epoch_count = 1;
    // average the mini-batch gradients (divide by opt.batch_size)
    float eta_div_nrows = opt.eta/opt.nrows;
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(setuptimer);
#endif
    const float scale_factor_one = 1.;
    mkl_set_num_threads(nthreads);
    while (iter_count < opt.max_iters) {
        // pseudo-uniform random sampling.
        // TODO(Aditya): make random sampling a user option.
        // sampled_row = ((static_cast<T>(std::rand())/ RAND_MAX) * opt.nrows);
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
        // cyclic sampling.
        startidx = (startidx >= opt.nrows) ? (0) : (startidx);
        current_batch_size = (iter_count + opt.batch_size - 1 >= epoch_count*opt.nrows) ? (last_batch_size) : (opt.batch_size);
        mkl_sparse_s_create_csr(&csr_sampledRows, SPARSE_INDEX_BASE_ZERO, current_batch_size, opt.ncols, rowptr + startidx, rowptr + startidx + 1, colptr, data);
        mkl_sparse_s_mv(SPARSE_OPERATION_NON_TRANSPOSE, scale_factor_one, csr_sampledRows, descr_sampledRows, weights, 0., residual);
        // for (unsigned int i = 0; i < opt.batch_size; ++i) {
        //     sampled_row[i] = (iter_count + i) % opt.nrows;
        // }

#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
        // compute the residual for row.
        // sparse_gemv(opt.batch_size, sampled_row, scale_factor_one,
        // rowptr, colptr, data, weights, residual);
        MPI_Allreduce(residual, residual_reduced, current_batch_size,
        get_MPI_Datatype(&residual[0]), MPI_SUM, comm);
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        for (unsigned int i = 0; i < current_batch_size; ++i) {
            residual[i] = scale_factor_one/
            (scale_factor_one
            + exp(labels[startidx + i]*residual_reduced[i]));
            residual[i] *= labels[startidx + i];
        }
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
        // compute the gradient.
        // sparse_gemv_transposed(opt.batch_size, sampled_row, scale_factor_one,
        // rowptr, colptr, data, residual, grad);
        // update the weights.
        // axpy(opt.ncols, eta_div_nrows, grad, weights);
        mkl_sparse_s_mv(SPARSE_OPERATION_TRANSPOSE, scale_factor_one, csr_sampledRows, descr_sampledRows, residual, 0., grad);
        cblas_saxpy(opt.ncols, eta_div_nrows, grad, 1, weights, 1);
#ifdef TAU_PROFILING
        TAU_PROFILE_START(misctimer);
#endif
        if ((iter_count+current_batch_size) / opt.nrows == epoch_count) {
            epoch_count++;
            loss = training_loss(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            accuracy = training_accuracy(comm, opt.nrows,
            rowptr, colptr, data, labels, weights);
            if (loss < 0. || accuracy < 0.) {
                std::cerr <<
                "[mini-batch logistic_regression_mkl]: loss/accuracy is negative."
                << std::endl;
                return;
            }
            if (mpi_rank == 0) {
                std::cout << "Epoch: " << (iter_count+current_batch_size)/opt.nrows
                << ", Iteration: " << iter_count+current_batch_size << ", Loss: "
                << std::setprecision(16) << loss << ", Training Accuracy: "
                << std::setprecision(6) << accuracy << "%" << std::endl;
            }
        }
        // reset residual, grad for next iteration.
        std::memset(grad, 0, sizeof(float)*opt.ncols);
        std::memset(residual, 0, sizeof(float)*current_batch_size);
        iter_count += current_batch_size;
        startidx += current_batch_size;
#ifdef TAU_PROFILING
        TAU_PROFILE_STOP(misctimer);
#endif
    }
#ifdef TAU_PROFILING
    TAU_PROFILE_START(misctimer);
#endif
    ALIGNED_FREE(residual);
    ALIGNED_FREE(residual_reduced);
    ALIGNED_FREE(grad);
#ifdef TAU_PROFILING
    TAU_PROFILE_STOP(misctimer);
#endif

}

template <typename T>
void ca_logistic_regression(MPI_Comm comm, Options<T> opt,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[CA_logistic_regression]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[CA_logistic_regression]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    std::memset(weights, 0, sizeof(T)*opt.ncols);
    unsigned int iter_count = 0;
    unsigned int gram_nvals = ((opt.s-1)*opt.s)/2;
    T* gram_matrix = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*(gram_nvals+opt.s)));
    T* gram_matrix_reduced = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*(gram_nvals+opt.s)));
    T* grad = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.ncols));
    if (gram_matrix == nullptr || gram_matrix_reduced == nullptr
    || grad == nullptr) {
        std::cerr <<
        "[CA_logistic_regression]: gram_matrix or grad pointers are nullptrs."
        << std::endl;
        return;
    }
    std::memset(gram_matrix, 0, sizeof(T)*gram_nvals);
    std::memset(gram_matrix_reduced, 0, sizeof(T)*(gram_nvals +opt.s));
    std::memset(grad, 0, sizeof(T)*opt.ncols);
    T residual_correction = 0.;
    T loss = -1.;
    T accuracy = 0.;
    const T scale_factor_one = 1.;
    T eta_div_nrows = opt.eta/opt.nrows;
    int* sampled_rows = static_cast<int*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*opt.s));
    T* sampled_labels = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.s));
    double start = 0., comm_time = 0., gram_matrix_time = 0., grad_correction_time = 0.;
    while (iter_count < opt.max_iters) {
    // sample s rows.
        for (unsigned int i = 0; i < opt.s; ++i) {
            // pseudo-uniform random sampling.
            // TODO(Aditya): make random sampling a user option.
            // sampled_rows[i] = (int)((static_cast<T>
            // (std::rand())/ RAND_MAX) * opt.nrows);

            // cyclic sampling.
            sampled_rows[i] = (iter_count+i) % opt.nrows;
            sampled_labels[i] = labels[sampled_rows[i]];
        }
        start = dsecnd();
        // compute s residuals.
        sparse_gemv(opt.s, sampled_rows, scale_factor_one,
        rowptr, colptr, data, weights, &gram_matrix[gram_nvals]);
        // compute Gram matrix.
        compute_gram_matrix(opt.s, sampled_rows, scale_factor_one,
        rowptr, colptr, data, gram_matrix);
        gram_matrix_time += dsecnd() - start;
        start = dsecnd();
        MPI_Allreduce(gram_matrix, gram_matrix_reduced, gram_nvals + opt.s,
        get_MPI_Datatype(gram_matrix), MPI_SUM, comm);
        comm_time += dsecnd() - start;
        // if(mpi_rank == 0){
        //     for(int i = 0; i < gram_nvals; ++i){
        //         std::cout << gram_matrix_reduced[i] << " ";
        //     }
        //     std::cout << std::endl;
        // }
        // if(mpi_rank == 0){
        //     for(unsigned int i = 0; i < gram_nvals; ++i){
        //         std::cout << gram_matrix_reduced[i] << " ";
        //     }
        //     std::cout << std::endl;
        // }
        // if(iter_count == opt.s*20){
        //     return;
        // }
        // perform s-step updates.
        start = dsecnd();
        unsigned int offset = 0;
        for (unsigned int i = 0; i < opt.s; ++i) {
            offset = (i*(i-1))/2;
            // correct the i-th residual based on previous solutions.
            for (unsigned int j = 0; j < i; ++j) {
                // if batch_size > 1,
                // need to use gemv for gram_matrix*residuals.
                residual_correction += eta_div_nrows*(sampled_labels[i]
                *sampled_labels[j]*gram_matrix_reduced[offset + j]
                *gram_matrix_reduced[gram_nvals + j]);
            }

            gram_matrix_reduced[gram_nvals + i] =
            static_cast<T>(1.)/ (static_cast<T>(1.)
            + exp(sampled_labels[i]*gram_matrix_reduced[gram_nvals + i]
            + residual_correction));
            sparse_scal(sampled_rows[i], sampled_labels[i]
            *gram_matrix_reduced[gram_nvals + i],
            rowptr, colptr, data, grad);
            // update the weights.
            axpy(opt.ncols, eta_div_nrows, grad, weights);
            // loss = training_loss(comm, opt.nrows,
            //         rowptr, colptr, data, labels, weights);
            // accuracy = training_accuracy(comm, opt.nrows,
            // rowptr, colptr, data, labels, weights);
            // if(mpi_rank == 0)
            //     std::cout << loss << ", " << accuracy << std::endl;
            // if(iter_count == 4)
            //     return;
            if (iter_count % opt.log_freq == opt.log_freq - 1) {
                    loss = training_loss(comm, opt.nrows,
                    rowptr, colptr, data, labels, weights);
                    accuracy = training_accuracy(comm, opt.nrows,
                    rowptr, colptr, data, labels, weights);
                    if (loss < 0. || accuracy < 0.) {
                        std::cerr <<
                        "[CA_logistic_regression]: loss/accuracy is negative."
                        << std::endl;
                        return;
                    }
                    if (mpi_rank == 0) {
                        std::cout << "Epoch: " << (iter_count+1)/opt.nrows
                        << ", Iteration: " << iter_count+1 << ", Loss: "
                        << std::setprecision(16) << loss
                        << ", Training Accuracy: "
                        << std::setprecision(6) << accuracy << "%" << std::endl;
                    }
            }
            iter_count++;
            if (iter_count == opt.max_iters) {
                grad_correction_time += dsecnd() - start;
                ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
                ALIGNED_FREE(grad);
                if(mpi_rank == 0){
                    std::cout << "Gram Matrix time: " << gram_matrix_time*1000 << std::endl;
                    std::cout << "MPI_Allreduce time: " << comm_time*1000 << std::endl;
                    std::cout << "s-step update time: " << grad_correction_time*1000 << std::endl;
                }
                return;
            }
            residual_correction = 0.;
            std::memset(grad, 0, sizeof(T)*opt.ncols);
        }
        grad_correction_time += dsecnd() - start;
        // reset residuals, grad, residual_correction for next iteration.
        std::memset(gram_matrix, 0, sizeof(T)*(gram_nvals+opt.s));
    }
    ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
    ALIGNED_FREE(grad);
    if(mpi_rank == 0){
        std::cout << "Gram Matrix time: " << gram_matrix_time*1000 << std::endl;
        std::cout << "MPI_Allreduce time: " << comm_time*1000 << std::endl;
        std::cout << "s-step update time: " << grad_correction_time*1000 << std::endl;
    }
}

void ca_logistic_regression_mkl(MPI_Comm comm, Options<float> opt,
int* rowptr, int* colptr, float* data, float* labels, float* weights, int nthreads) {
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[CA_logistic_regression_mkl]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[CA_logistic_regression_mkl]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    std::memset(weights, 0, sizeof(float)*opt.ncols);
    unsigned int iter_count = 0;
    unsigned int last_batch_s = opt.nrows % opt.s;
    unsigned int last_batch_startidx = (opt.nrows / opt.s) * opt.s;
    unsigned int startidx = 0;
    unsigned int s = opt.s;
    // unsigned int gram_nvals = opt.s*opt.s;
    unsigned int nnz_row;
    unsigned int gram_nvals = ((opt.s-1)*opt.s)/2;
    unsigned int full_batch_gram_nvals = gram_nvals;
    unsigned int last_batch_gram_nvals = ((last_batch_s-1)*last_batch_s)/2;
    unsigned int idx = 0;
    float* syrkd_buffer = static_cast<float*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*(opt.s*opt.s)));

    float* gram_matrix = static_cast<float*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*(gram_nvals+opt.s)));
    float* gram_matrix_reduced = static_cast<float*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*(gram_nvals+opt.s)));
    float* grad = static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.ncols));
    if (gram_matrix == nullptr || gram_matrix_reduced == nullptr
    || grad == nullptr) {
        std::cerr <<
        "[CA_logistic_regression_mkl]: gram_matrix or grad pointers are nullptrs."
        << std::endl;
        return;
    }
    std::memset(gram_matrix, 0, sizeof(float)*gram_nvals);
    std::memset(gram_matrix_reduced, 0, sizeof(float)*(gram_nvals +opt.s));
    std::memset(syrkd_buffer, 0, sizeof(float)*(opt.s*opt.s));
    std::memset(grad, 0, sizeof(float)*opt.ncols);
    float residual_correction = 0.;
    float loss = -1.;
    float accuracy = 0.;
    const float scale_factor_one = 1.;
    float eta_div_nrows = opt.eta/opt.nrows;
    sparse_matrix_t csr_sampledRows;
    sparse_status_t spblas_err;
    matrix_descr descr_sampledRows;
    descr_sampledRows.type = SPARSE_MATRIX_TYPE_GENERAL;
    descr_sampledRows.mode = SPARSE_FILL_MODE_FULL;
    descr_sampledRows.diag = SPARSE_DIAG_NON_UNIT;
    int* sampled_rows = static_cast<int*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*opt.s));
    float* sampled_labels = static_cast<float*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.s));
    double start = 0., comm_time = 0., gram_matrix_time = 0., grad_correction_time = 0.;
    double create_csr_time = 0.;
    mkl_set_num_threads(nthreads);
    while (iter_count < opt.max_iters) {
        // sample min(s, nrows - iter_count*s % nrows) rows.
        // for (unsigned int i = 0; i < opt.s; ++i) {
        //     // pseudo-uniform random sampling.
        //     // TODO(Aditya): make random sampling a user option.
        //     // sampled_rows[i] = (int)((static_cast<float>
        //     // (std::rand())/ RAND_MAX) * opt.nrows);

        //     // cyclic sampling.
        //     sampled_rows[i] = (iter_count+i) % opt.nrows;
        //     sampled_labels[i] = labels[sampled_rows[i]];
        // }
        startidx = (startidx >= opt.nrows) ? 0 : startidx;
        s = (startidx + opt.s >= opt.nrows) ? (last_batch_s) : (opt.s);
        gram_nvals = (startidx + opt.s >= opt.nrows) ? (last_batch_gram_nvals) : (full_batch_gram_nvals);
        start = dsecnd();
        // compute s residuals.
        mkl_sparse_s_create_csr(&csr_sampledRows, SPARSE_INDEX_BASE_ZERO, s, opt.ncols, rowptr + startidx, rowptr + startidx + 1, colptr, data);
        create_csr_time += dsecnd() - start;
        mkl_sparse_s_syrkd(SPARSE_OPERATION_NON_TRANSPOSE, csr_sampledRows, scale_factor_one, 0., syrkd_buffer, SPARSE_LAYOUT_COLUMN_MAJOR, s);
        mkl_sparse_s_mv(SPARSE_OPERATION_NON_TRANSPOSE, scale_factor_one, csr_sampledRows, descr_sampledRows, weights, 0., gram_matrix + gram_nvals);
        idx = 0;
        for(unsigned int i = 1; i < s; ++i){
            for(unsigned int j = 0; j < i; ++j){
                gram_matrix[idx++] = syrkd_buffer[i*s + j];
            }
        }
        // sparse_gemv(opt.s, sampled_rows, scale_factor_one,
        // rowptr, colptr, data, weights, &gram_matrix[gram_nvals]);
        // compute Gram matrix.
        // compute_gram_matrix(opt.s, sampled_rows, scale_factor_one,
        // rowptr, colptr, data, gram_matrix);
        gram_matrix_time += dsecnd() - start;
        start = dsecnd();
        MPI_Allreduce(gram_matrix, gram_matrix_reduced, gram_nvals + s,
        get_MPI_Datatype(gram_matrix), MPI_SUM, comm);
        comm_time += dsecnd() - start;
        // if(mpi_rank == 0){
        //     for(int i = 0; i < gram_nvals; ++i){
        //         std::cout << gram_matrix_reduced[i] << " ";
        //     }
        //     std::cout << std::endl;
        // }
        // if(iter_count == 20*opt.s){
        //     MPI_Finalize();
        //     exit(0);
        // }
        // perform s-step updates.
        unsigned int offset = 0;
        start = dsecnd();
        for (unsigned int i = 0; i < s; ++i) {
            offset = (i * (i - 1))/2;
            // correct the i-th residual based on previous solutions.
            for (unsigned int j = 0; j < i; ++j) {
                // if batch_size > 1,
                // need to use gemv for gram_matrix*residuals.
                residual_correction += eta_div_nrows*(labels[startidx + i]
                *labels[startidx + j]*gram_matrix_reduced[offset + j]
                *gram_matrix_reduced[gram_nvals + j]);
            }

            gram_matrix_reduced[gram_nvals + i] =
            scale_factor_one / (scale_factor_one
            + exp(labels[startidx + i]*gram_matrix_reduced[gram_nvals + i]
            + residual_correction));
            nnz_row = rowptr[startidx + i + 1] - rowptr[startidx + i];
            cblas_saxpyi(nnz_row, labels[startidx + i]*gram_matrix_reduced[gram_nvals + i], data + rowptr[startidx + i], colptr + rowptr[startidx + i], grad);
            cblas_saxpy(opt.ncols, eta_div_nrows, grad, 1, weights, 1);

            // sparse_scal(sampled_rows[i], sampled_labels[i]
            // *gram_matrix_reduced[gram_nvals + i],
            // rowptr, colptr, data, grad);
            // // update the weights.
            // axpy(opt.ncols, eta_div_nrows, grad, weights);
            // loss = training_loss(comm, opt.nrows,
            //         rowptr, colptr, data, labels, weights);
            // accuracy = training_accuracy(comm, opt.nrows,
            //         rowptr, colptr, data, labels, weights);
            // if(mpi_rank == 0)
            //     std::cout << loss << ", " << accuracy << std::endl;
            // if(iter_count == 4){
            //     MPI_Finalize();
            //     exit(0);
            // }
            if (iter_count % opt.log_freq == opt.log_freq - 1) {
                    loss = training_loss(comm, opt.nrows,
                    rowptr, colptr, data, labels, weights);
                    accuracy = training_accuracy(comm, opt.nrows,
                    rowptr, colptr, data, labels, weights);
                    if (loss < 0. || accuracy < 0.) {
                        std::cerr <<
                        "[CA_logistic_regression_mkl]: loss/accuracy is negative."
                        << std::endl;
                        return;
                    }
                    if (mpi_rank == 0) {
                        std::cout << "Epoch: " << (iter_count+1)/opt.nrows
                        << ", Iteration: " << iter_count+1 << ", Loss: "
                        << std::setprecision(16) << loss
                        << ", Training Accuracy: "
                        << std::setprecision(6) << accuracy << "%" << std::endl;
                    }
            }
            iter_count++;
            if (iter_count == opt.max_iters) {
                grad_correction_time += dsecnd() - start;
                if(mpi_rank == 0){
                    std::cout << "Create CSR time: " << create_csr_time*1000 << std::endl;
                    std::cout << "Gram Matrix time: " << gram_matrix_time*1000 << std::endl;
                    std::cout << "MPI_Allreduce time: " << comm_time*1000 << std::endl;
                    std::cout << "s-step update time: " << grad_correction_time*1000 << std::endl;
                }
                ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
                ALIGNED_FREE(grad);
                return;
            }
            residual_correction = 0.;
            std::memset(grad, 0, sizeof(float)*opt.ncols);
        }
        grad_correction_time += dsecnd() - start;
        // reset residuals, grad, residual_correction for next iteration.
        std::memset(gram_matrix, 0, sizeof(float)*(gram_nvals+s));
        std::memset(syrkd_buffer, 0, sizeof(float)*(opt.s*opt.s));
        startidx += s;
    }
    ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
    ALIGNED_FREE(grad);
    if(mpi_rank == 0){
        std::cout << "Create CSR time: " << create_csr_time*1000 << std::endl;
        std::cout << "Gram Matrix time: " << gram_matrix_time*1000 << std::endl;
        std::cout << "MPI_Allreduce time: " << comm_time*1000 << std::endl;
        std::cout << "s-step update time: " << grad_correction_time*1000 << std::endl;
    }

}


template <typename T>
void mini_batch_ca_logistic_regression(MPI_Comm comm, Options<T> opt,
int* rowptr, int* colptr, T* data, T* labels, T* weights) {
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[mini_batch_CA_logistic_regression]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[mini_batch_CA_logistic_regression]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    std::memset(weights, 0, sizeof(T)*opt.ncols);
    // std::fill((weights).begin(), (weights).end(), 0.);
    unsigned int iter_count = 0;
    unsigned int gram_nvals =
    (((opt.s-1)*opt.s)/2)*opt.batch_size*opt.batch_size;
    unsigned int redundant_gram_nvals =
    (((opt.s*opt.batch_size)-1)*(opt.s*opt.batch_size))/2;
    T* gram_matrix = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*(gram_nvals+(opt.s*opt.batch_size))));
    T* gram_matrix_reduced = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*(gram_nvals+(opt.s*opt.batch_size))));
    T* redundant_gram_matrix = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*(redundant_gram_nvals)));
    if (mpi_rank == 0) {
        std::cout << "MPI_Allreduce size: "
        << (gram_nvals + (opt.s*opt.batch_size))*sizeof(T)
        << " bytes." << std::endl;
    }
    T* grad = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.ncols));
    T * residual_correction = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN,
    sizeof(T)*opt.batch_size));
    if (gram_matrix == nullptr || gram_matrix_reduced == nullptr
    || grad == nullptr || residual_correction == nullptr) {
        std::cerr <<
        "[mini_batch_CA_logistic_regression]: "
        << "gram_matrix or grad pointers are nullptrs."
        << std::endl;
        return;
    }
    std::memset(redundant_gram_matrix, 0, sizeof(T)*redundant_gram_nvals);
    std::memset(gram_matrix, 0,
    sizeof(T)*(gram_nvals + (opt.s*opt.batch_size)));
    std::memset(gram_matrix_reduced, 0,
    sizeof(T)*(gram_nvals + (opt.s*opt.batch_size)));
    std::memset(grad, 0, sizeof(T)*opt.ncols);
    std::memset(residual_correction, 0, sizeof(T)*opt.batch_size);

    T loss = -1.;
    T accuracy = 0.;
    const T scale_factor_one = 1.;
    T eta_div_nrows = opt.eta/opt.nrows;
    int* sampled_rows = static_cast<int*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*opt.s*opt.batch_size));
    T* sampled_labels = static_cast<T*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*opt.s*opt.batch_size));
    unsigned int epoch_count = 1;
    while (iter_count < opt.max_iters) {
    // sample s*opt.batch_size rows.
        for (unsigned int i = 0; i < opt.s*opt.batch_size; ++i) {
            // pseudo-uniform random sampling.
            // sampled_rows[i] = (int)((static_cast<T>
            // (std::rand())/ RAND_MAX) * opt.nrows);

            // cyclic sampling.
            sampled_rows[i] = (iter_count+i) % opt.nrows;
            sampled_labels[i] = labels[sampled_rows[i]];
        }
        sparse_gemv(opt.s*opt.batch_size, sampled_rows, scale_factor_one,
        rowptr, colptr, data, weights, &gram_matrix[gram_nvals]);

        blocked_compute_gram_matrix(opt.s, opt.batch_size,
        sampled_rows, scale_factor_one,
        rowptr, colptr, data, gram_matrix);

        // if(mpi_rank == 0){
        //     for(int k = 0; k < (opt.s*(opt.s-1))/2; ++k){
        //         for(unsigned int i = 0; i < opt.batch_size; ++i){
        //             for(unsigned int j = 0; j < opt.batch_size; ++j){
        //                 std::cout << gram_matrix[k*opt.batch_size*opt.batch_size + opt.batch_size*i + j] << ", ";
        //             }
        //             std::cout << std::endl;
        //         }
        //         std::cout << std::endl << std::endl;
        //     }
        // }
        MPI_Allreduce(gram_matrix, gram_matrix_reduced,
        gram_nvals + opt.batch_size*opt.s,
        get_MPI_Datatype(gram_matrix), MPI_SUM, comm);

        // perform s-step updates.
        unsigned int offset = 0;
        for (unsigned int i = 0; i < opt.s; ++i) {
            offset = (i*(i-1))/2;
            // correct the i-th residual based on previous solutions.
            for (unsigned int j = 0; j < i; ++j) {
                    // if batch_size > 1,
                    // need to use gemv for gram_matrix*residuals.
                dense_gemv(opt.batch_size,
                eta_div_nrows, &sampled_labels[i*opt.batch_size],
                &sampled_labels[j*opt.batch_size],
                &gram_matrix_reduced[(j + offset)
                *opt.batch_size*opt.batch_size],
                &gram_matrix_reduced[gram_nvals + j*opt.batch_size],
                residual_correction);
                // if(mpi_rank == 0){
                //     for(unsigned int k = 0; k < opt.batch_size; ++k){
                //         std::cout << residual_correction[k] << ", ";
                //     }
                //     std::cout << std::endl;
                // }
                // if(i == 2){
                //     return;
                // }
            }
            for (unsigned int j = 0; j < opt.batch_size; ++j) {
                gram_matrix_reduced[gram_nvals + i*opt.batch_size + j] =
                static_cast<T>(1.)/(static_cast<T>(1.)
                + exp(sampled_labels[i*opt.batch_size + j]
                * gram_matrix_reduced[gram_nvals + i*opt.batch_size + j]
                + residual_correction[j]));
                residual_correction[j] =
                gram_matrix_reduced[gram_nvals + i*opt.batch_size + j]
                * sampled_labels[i*opt.batch_size + j];
            }
            // if(mpi_rank == 0){
            //     for(unsigned int k = 0; k < opt.batch_size; ++k){
            //         std::cout << residual_correction[k] << ", ";
            //     }
            // }
            sparse_gemv_transposed(opt.batch_size,
            &sampled_rows[i*opt.batch_size],
            scale_factor_one, rowptr, colptr, data,
            residual_correction, grad);

            // update the weights.
            axpy(opt.ncols, eta_div_nrows, grad, weights);
            // if(mpi_rank == 0){
            //     for(unsigned int k = 0; k < 10; ++k){
            //         std::cout << residual_correction[k] << ", ";
            //     }
            //     std::cout << std::endl;
            // }
            // if(iter_count+opt.batch_size == 6*opt.batch_size){return;}
            if ((iter_count+opt.batch_size) / opt.nrows == epoch_count) {
                    epoch_count++;
                    loss = training_loss(comm, opt.nrows,
                    rowptr, colptr, data, labels, weights);
                    accuracy = training_accuracy(comm, opt.nrows,
                    rowptr, colptr, data, labels, weights);
                    if (loss < 0. || accuracy < 0.) {
                        std::cerr << "[mini_batch_CA_logistic_regression]: "
                        << "loss/accuracy is negative." << std::endl;
                        return;
                    }
                    if (mpi_rank == 0) {
                        std::cout << "Epoch: "
                        << (iter_count+opt.batch_size)/opt.nrows
                        << ", Iteration: " << iter_count+opt.batch_size
                        << ", Loss: " << std::setprecision(16) << loss
                        << ", Training Accuracy: " << std::setprecision(6)
                        << accuracy << "%" << std::endl;
                    }
            }
            iter_count += opt.batch_size;
            if (iter_count == opt.max_iters) {
                ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
                ALIGNED_FREE(grad);
                ALIGNED_FREE(residual_correction), ALIGNED_FREE(sampled_rows);
                return;
            }
            std::memset(residual_correction, 0, sizeof(T)*opt.batch_size);
            std::memset(grad, 0, sizeof(T)*opt.ncols);

        }
        // if(mpi_rank == 0){std::cout << std::endl;}
        // if(iter_count == 6*opt.s*opt.batch_size){return;}

        // reset residuals, grad, residual_correction for next iteration.
        std::memset(gram_matrix, 0,
        sizeof(T)*(gram_nvals+opt.s*opt.batch_size));
    }
    ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
    ALIGNED_FREE(grad);
    ALIGNED_FREE(residual_correction), ALIGNED_FREE(sampled_rows);
}

void mini_batch_ca_logistic_regression_mkl(MPI_Comm comm, Options<float> opt,
int* rowptr, int* colptr, float* data, float* labels, float* weights, int nthreads) {
    if (rowptr == nullptr || colptr == nullptr || data == nullptr
    || labels == nullptr || weights == nullptr) {
        std::cerr <<
        "[mini_batch_CA_logistic_regression_mkl]: pointers are nullptrs."
        << std::endl;
        return;
    }
    if (opt.nrows <= 0 || opt.ncols <= 0 || opt.nnz <= 0 || opt.max_iters <= 0
    || opt.s < 1 || opt.chkpt_freq <= 0 || opt.chkpt_freq >= opt.max_iters
    || opt.log_freq <= 0 || opt.batch_size <= 0
    || opt.batch_size >= opt.nrows || opt.tol <= 0. || opt.eta <= 0.) {
        std::cerr <<
        "[mini_batch_CA_logistic_regression_mkl]: Options settings are incorrect."
        << std::endl;
        return;
    }
    int mpi_rank, mpi_size;
    MPI_Comm_rank(comm, &mpi_rank);
    MPI_Comm_size(comm, &mpi_size);
    std::srand(opt.seed);
    std::memset(weights, 0, sizeof(float)*opt.ncols);
    // std::fill((weights).begin(), (weights).end(), 0.);
    unsigned int iter_count = 0;
    unsigned int gram_nvals =
    (((opt.s-1)*opt.s)/2)*opt.batch_size*opt.batch_size;
    unsigned int redundant_gram_nvals =
    (((opt.s*opt.batch_size)-1)*(opt.s*opt.batch_size))/2;
    float* gram_matrix = static_cast<float*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*(gram_nvals+(opt.s*opt.batch_size))));
    float* gram_matrix_reduced = static_cast<float*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*(gram_nvals+(opt.s*opt.batch_size))));

    float* syrkd_buffer = static_cast<float*>
    (ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*(opt.batch_size*opt.batch_size*opt.s*opt.s)));
    std::memset(syrkd_buffer, 0, sizeof(float)*(opt.s*opt.s*opt.batch_size*opt.batch_size));

    if (mpi_rank == 0) {
        std::cout << "MPI_Allreduce size: "
        << (gram_nvals + (opt.s*opt.batch_size))*sizeof(float)
        << " bytes." << std::endl;
    }
    float* grad = static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(float)*opt.ncols));
    float * residual_correction = static_cast<float*>(ALIGNED_ALLOC(MEM_ALIGN,
    sizeof(float)*opt.s*opt.batch_size));
    if (gram_matrix == nullptr || gram_matrix_reduced == nullptr
    || grad == nullptr || residual_correction == nullptr) {
        std::cerr <<
        "[mini_batch_CA_logistic_regression_mkl]: "
        << "gram_matrix or grad pointers are nullptrs."
        << std::endl;
        return;
    }
    std::memset(gram_matrix, 0,
    sizeof(float)*(gram_nvals + (opt.s*opt.batch_size)));
    std::memset(gram_matrix_reduced, 0,
    sizeof(float)*(gram_nvals + (opt.s*opt.batch_size)));
    std::memset(grad, 0, sizeof(float)*opt.ncols);
    std::memset(residual_correction, 0, sizeof(float)*opt.s*opt.batch_size);

    float loss = -1.;
    float accuracy = 0.;
    const float scale_factor_one = 1.;
    float eta_div_nrows = opt.eta/opt.nrows;

    sparse_matrix_t csr_sampledRows;
    sparse_status_t spblas_err;
    matrix_descr descr_sampledRows;
    descr_sampledRows.type = SPARSE_MATRIX_TYPE_GENERAL;
    descr_sampledRows.mode = SPARSE_FILL_MODE_FULL;
    descr_sampledRows.diag = SPARSE_DIAG_NON_UNIT;
    unsigned int epoch_count = 1;
    unsigned int current_batch_size = opt.batch_size;
    unsigned int last_batch_size = opt.nrows % opt.batch_size;
    unsigned int current_s = opt.s;
    unsigned int startidx = 0;
    unsigned int idx;
    if(mpi_rank == 0)
        std::cout << "last_batch_size: " << last_batch_size << std::endl;
    double start = 0., create_csr_time = 0.;
    mkl_set_num_threads(nthreads);
    while (iter_count < opt.max_iters) {
    // sample s*opt.batch_size rows.
        // for (unsigned int i = 0; i < opt.s*opt.batch_size; ++i) {
        //     // pseudo-uniform random sampling.
        //     // sampled_rows[i] = (int)((static_cast<float>
        //     // (std::rand())/ RAND_MAX) * opt.nrows);

        //     // cyclic sampling.
        //     sampled_rows[i] = (iter_count+i) % opt.nrows;
        //     sampled_labels[i] = labels[sampled_rows[i]];
        // }
        current_s = opt.s;
        current_batch_size = (startidx + opt.batch_size >= opt.nrows) ? last_batch_size : opt.batch_size;
        while(startidx + current_s*current_batch_size > opt.nrows){
            current_s--;
        }
        // std::cout << "current_batch_size: " << current_batch_size << "current_s: " << current_s << std::endl;
        // current_s = (current_batch_size < opt.batch_size) ? (1) : opt.s;
        startidx = (startidx >= opt.nrows) ? 0 : startidx;
        // current_batch_size = (iter_count + opt.s*opt.batch_size - 1 >= epoch_count*opt.nrows) ? last_batch_size : opt.batch_size;

        start = dsecnd();
        // compute s residuals.
        mkl_sparse_s_create_csr(&csr_sampledRows, SPARSE_INDEX_BASE_ZERO, current_batch_size*current_s, opt.ncols, rowptr + startidx, rowptr + startidx + 1, colptr, data);
        create_csr_time += dsecnd() - start;
        mkl_sparse_s_syrkd(SPARSE_OPERATION_NON_TRANSPOSE, csr_sampledRows, scale_factor_one, 0., syrkd_buffer, SPARSE_LAYOUT_COLUMN_MAJOR, current_batch_size*current_s);
        mkl_sparse_s_mv(SPARSE_OPERATION_NON_TRANSPOSE, scale_factor_one, csr_sampledRows, descr_sampledRows, weights, 0., gram_matrix + gram_nvals);
        idx = 0;
        for(unsigned int i = 1; i < current_s; ++i){
            for(unsigned int j = 0; j < i; ++j){
                for(unsigned int k = 0; k < current_batch_size; ++k){
                    std::memcpy(gram_matrix + idx, syrkd_buffer + i*current_s*current_batch_size*current_batch_size + j*current_batch_size + k*current_s*current_batch_size, sizeof(float)*current_batch_size);
                    // gram_matrix + idx = syrkd_buffer[i*current_s + j];
                    idx += current_batch_size;
                }
            }
        }

        // if(mpi_rank == 0){
        //     for(unsigned int k = 0; k < (current_s*(current_s - 1))/2; ++k){
        //         for(unsigned int i = 0; i < current_batch_size; ++i){
        //             for(unsigned int j = 0; j < current_batch_size; ++j){
        //                 std::cout << gram_matrix[k*current_batch_size*current_batch_size + current_batch_size*j + i] << ", ";
        //             }
        //             std::cout << std::endl;
        //         }
        //         std::cout << std::endl << std::endl;
        //     }
        //     // std::cout << "syrkd_buffer" << std::endl;
        //     // for(unsigned int i = 0; i < current_s*current_batch_size; ++i){
        //     //     for(unsigned int j = 0; j < current_s*current_batch_size; ++j){
        //     //         std::cout << syrkd_buffer[current_s*current_batch_size*j + i] << ", ";
        //     //     }
        //     //     std::cout << std::endl;
        //     // }
        // }
        // sparse_gemv(opt.s*opt.batch_size, sampled_rows, scale_factor_one,
        // rowptr, colptr, data, weights, &gram_matrix[gram_nvals]);

        // blocked_compute_gram_matrix(opt.s, opt.batch_size,
        // sampled_rows, scale_factor_one,
        // rowptr, colptr, data, gram_matrix);
        MPI_Allreduce(gram_matrix, gram_matrix_reduced,
        gram_nvals + current_batch_size*current_s,
        get_MPI_Datatype(gram_matrix), MPI_SUM, comm);

        // perform s-step updates.
        unsigned int offset = 0;
        for (unsigned int i = 0; i < current_s; ++i) {
            offset = (i*(i-1))/2;
            // correct the i-th residual based on previous solutions.
            for (unsigned int j = 0; j < i; ++j) {
                    // if batch_size > 1,
                    // need to use gemv for gram_matrix*residuals.
                // cblas_sgemv(CblasColMajor, CblasNoTrans, current_batch_size, current_batch_size

                /*TODO: multiply rows and cols of gram_matrix_reduced by corresponding labels prior to cblas_sgemv call!*/
                scale_gram_matrix(current_batch_size, labels + startidx + j*current_batch_size, labels + startidx + i*current_batch_size, gram_matrix_reduced + ((j + offset)*current_batch_size*current_batch_size));

                // dense_gemv(current_batch_size,
                // eta_div_nrows, &sampled_labels[i*current_batch_size],
                // &sampled_labels[j*current_batch_size],
                // &gram_matrix_reduced[(j + offset)
                // *current_batch_size*current_batch_size],
                // &gram_matrix_reduced[gram_nvals + j*current_batch_size],
                // residual_correction);
                cblas_sgemv(CblasColMajor, CblasTrans, current_batch_size, current_batch_size, eta_div_nrows, gram_matrix_reduced + ((j+offset)*current_batch_size*current_batch_size), current_batch_size, gram_matrix_reduced + gram_nvals + j*current_batch_size, 1, 1., residual_correction + i*current_batch_size, 1);
                // cblas_saxpy(current_batch_size, scale_factor_one, residual_correction + current_batch_size*(i-1), 0, residual_correction + current_batch_size*i, 1);
                // if(mpi_rank == 0){
                //     for(unsigned int k = 0; k < current_batch_size; ++k){
                //         std::cout << residual_correction[k] << ", ";
                //     }
                //     std::cout << std::endl;
                // }
                // if(i == 2){
                //     return;
                // }
            }
            for (unsigned int j = 0; j < current_batch_size; ++j) {
                gram_matrix_reduced[gram_nvals + i*current_batch_size + j] =
                scale_factor_one/(scale_factor_one
                + exp(labels[startidx + i*current_batch_size + j]
                * gram_matrix_reduced[gram_nvals + i*current_batch_size + j]
                + residual_correction[i*current_batch_size + j]));
                residual_correction[i*current_batch_size + j] =
                gram_matrix_reduced[gram_nvals + i*current_batch_size + j]
                * labels[startidx + i*current_batch_size + j];
            }
        }
            // sparse_gemv_transposed(current_batch_size,
            // &sampled_rows[i*current_batch_size],
            // scale_factor_one, rowptr, colptr, data,
            // residual_correction, grad);

        // if(mpi_rank == 0){
        //     for(unsigned int k = 0; k < current_s*current_batch_size; ++k){
        //         std::cout << residual_correction[k] << ", ";
        //     }
        //     std::cout << std::endl;
        // }
        mkl_sparse_s_mv(SPARSE_OPERATION_TRANSPOSE, scale_factor_one, csr_sampledRows, descr_sampledRows, residual_correction, 0., grad);
        // update the weights.
        cblas_saxpy(opt.ncols, eta_div_nrows, grad, 1, weights, 1);
        // if(mpi_rank == 0){
        //     for(unsigned int k = 0; k < 10; ++k){
        //             std::cout << residual_correction[k] << ", ";
        //     }
        //     std::cout << std::endl;
        // }
        // if(iter_count+current_batch_size == 6*opt.batch_size){return;}
        // axpy(opt.ncols, eta_div_nrows, grad, weights);
        if ((iter_count+(current_s*current_batch_size)) / opt.nrows == epoch_count) {
                epoch_count++;
                // std::cout << "current_batch_size: " << current_batch_size << " current_s: " << current_s << std::endl;
                loss = training_loss(comm, opt.nrows,
                rowptr, colptr, data, labels, weights);
                accuracy = training_accuracy(comm, opt.nrows,
                rowptr, colptr, data, labels, weights);
                if (loss < 0. || accuracy < 0.) {
                    std::cerr << "[mini_batch_CA_logistic_regression_mkl]: "
                    << "loss/accuracy is negative." << std::endl;
                    return;
                }
                if (mpi_rank == 0) {
                    std::cout << "Epoch: "
                    << (iter_count+ current_s*current_batch_size)/opt.nrows
                    << ", Iteration: " << iter_count+(current_s*current_batch_size)
                    << ", Loss: " << std::setprecision(16) << loss
                    << ", Training Accuracy: " << std::setprecision(6)
                    << accuracy << "%" << std::endl;
                }
        }
        iter_count += current_s*current_batch_size;
        startidx += current_s*current_batch_size;
        if (iter_count == opt.max_iters) {
            ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
            ALIGNED_FREE(grad);
            ALIGNED_FREE(residual_correction);
            ALIGNED_FREE(syrkd_buffer);
            mkl_sparse_destroy(csr_sampledRows);
            return;
        }
        std::memset(residual_correction, 0, sizeof(float)*opt.s*opt.batch_size);
        std::memset(grad, 0, sizeof(float)*opt.ncols);
        // reset residuals, grad, residual_correction for next iteration.
        std::memset(gram_matrix, 0,
        sizeof(float)*(gram_nvals+opt.s*opt.batch_size));
    }
    ALIGNED_FREE(gram_matrix), ALIGNED_FREE(gram_matrix_reduced);
    ALIGNED_FREE(grad);
    ALIGNED_FREE(residual_correction);
    ALIGNED_FREE(syrkd_buffer);
    mkl_sparse_destroy(csr_sampledRows);
}

void scale_gram_matrix(unsigned int ldGram, float* labels_row, float* labels_col, float* gram_matrix){
    if(ldGram == 0 || labels_row == nullptr || labels_col == nullptr || gram_matrix == nullptr){
        std::cerr << "[scale_gram_matrix]: ldGram == 0 or pointers are nullptrs." << std::endl;
        return;
    }
    for(unsigned int i = 0; i < ldGram; ++i){
        for(unsigned int j = 0; j < ldGram; ++j){
            gram_matrix[j + i * ldGram] *= labels_row[i]*labels_col[j];
        }
    }
}

template <typename T>
void sparse_axpy(int sampled_rowidx, T alpha,
int* rowptr, int* colptr, T* data, T* out_vec) {
    if (sampled_rowidx < 0 || rowptr == nullptr || colptr == nullptr
    || data == nullptr || out_vec == nullptr) {
        std::cerr <<
        "[sparse_axpy]: sampled_rowidx < 0 or pointers are nullptrs."
        << std::endl;
        return;
    }
    int row_startidx = rowptr[sampled_rowidx];
    int row_endidx = rowptr[sampled_rowidx+1];
    for (int i = row_startidx; i < row_endidx; ++i) {
        out_vec[colptr[i]] += alpha*data[i];
    }
}

template <typename T>
void axpy(unsigned int out_vec_len, T alpha, T* in_vec, T* out_vec) {
    if (out_vec_len <= 0 || in_vec == nullptr || out_vec == nullptr) {
        std::cerr <<
        "[axpy]: out_vec_len < 0 or pointers are nullptrs."
        << std::endl;
        return;
    }
    for (unsigned int i = 0; i < out_vec_len; ++i) {
        out_vec[i] += alpha*in_vec[i];
    }
}

template <typename T>
void sparse_scal(int sampled_rowidx, T alpha,
int* rowptr, int* colptr, T* data, T* out_vec) {
    if (sampled_rowidx < 0 || rowptr == nullptr || colptr == nullptr
    || data == nullptr || out_vec == nullptr) {
        std::cerr <<
        "[sparse_scal]: sampled_rowidx < 0 or pointers are nullptrs."
        << std::endl;
        return;
    }
    int row_startidx = rowptr[sampled_rowidx];
    int row_endidx = rowptr[sampled_rowidx+1];
    for (int i = row_startidx; i < row_endidx; ++i) {
        out_vec[colptr[i]] = alpha*data[i];
    }
}

template <typename T>
void scal(unsigned int out_vec_len, T alpha, T* out_vec) {
    if (out_vec_len <= 0 || out_vec == nullptr) {
        std::cerr <<
        "[scal]: out_vec_len <= 0 or out_vec is nullptr."
        << std::endl;
        return;
    }
    for (unsigned int i = 0; i < out_vec_len; ++i) {
            out_vec[i] *= alpha;
    }
}

template <typename T>
void sparse_dot(int sampled_rowidx,
int* rowptr, int* colptr, T* data, T* in_vec, T* out_scalar) {
    if (sampled_rowidx < 0 || rowptr == nullptr || colptr == nullptr
    || data == nullptr || in_vec == nullptr || out_scalar == nullptr ) {
        std::cerr <<
        "[sparse_dot]: sampled_rowidx < 0 or pointers are nullptrs."
        << std::endl;
        return;
    }
    *out_scalar = 0.;
    int row_startidx = rowptr[sampled_rowidx];
    int row_endidx = rowptr[sampled_rowidx+1];
    for (int i = row_startidx; i < row_endidx; ++i) {
        *out_scalar += data[i]*in_vec[colptr[i]];
    }
}

template <typename T>
void dot(unsigned int nvals, T* row_vec, T* col_vec, T* dotprod) {
    if (nvals <= 0 || row_vec == nullptr || col_vec == nullptr
    || dotprod == nullptr) {
        std::cerr <<
        "[dot]: nvals <= 0 or pointers are nullptrs."
        << std::endl;
        return;
    }
    *dotprod = 0.;
    for (unsigned int i = 0; i < nvals; ++i) {
        *dotprod += row_vec[i]*col_vec[i];
    }
}

template <typename T>
void dot_unrolled(unsigned int nvals, T* row_vec, T* col_vec, T* dotprod) {
    if (nvals <= 0 || row_vec == nullptr || col_vec == nullptr
    || dotprod == nullptr) {
        std::cerr <<
        "[dot_unrolled]: nvals <= 0 or pointers are nullptrs." << std::endl;
        return;
    }
    const unsigned int LOOP_TILE_SIZE = 10;
    unsigned int remainder = nvals % LOOP_TILE_SIZE;
    *dotprod = 0.;
    for (unsigned int i = 0; i < remainder; ++i) {
        *dotprod += row_vec[i]*col_vec[i];
    }
    for (unsigned int i = remainder; i < nvals; i+=LOOP_TILE_SIZE) {
        *dotprod += row_vec[i]*col_vec[i] +
        row_vec[i+1]*col_vec[i+1] +
        row_vec[i+2]*col_vec[i+2] +
        row_vec[i+3]*col_vec[i+3] +
        row_vec[i+4]*col_vec[i+4] +
        row_vec[i+5]*col_vec[i+5] +
        row_vec[i+6]*col_vec[i+6] +
        row_vec[i+7]*col_vec[i+7] +
        row_vec[i+8]*col_vec[i+8] +
        row_vec[i+9]*col_vec[i+9];
    }
}

template <typename T>
void gemv(unsigned int nrows, T alpha,
int* rowptr, int* colptr, T* data, T* in_vec, T* out_vec) {
    if (nrows <= 0 || rowptr == nullptr || colptr == nullptr || data == nullptr
    || in_vec == nullptr || out_vec == nullptr) {
        std::cerr <<
        "[gemv]: nrows <= 0 or pointers are nullptrs." << std::endl;
        return;
    }
    for (unsigned int i = 0; i < nrows; ++i) {
        for (int j = rowptr[i]; j < rowptr[i+1]; ++j) {
            out_vec[i] += data[j]*in_vec[colptr[j]];
        }
        out_vec[i] *= alpha;
    }
}

template <typename T>
void sparse_gemv(unsigned int s, int* sampled_rowidxs, T alpha,
int* rowptr, int* colptr, T* data, T* in_vec, T* out_vec) {
    if (s < 1 || sampled_rowidxs == nullptr || rowptr == nullptr
    || colptr == nullptr || data == nullptr || in_vec == nullptr
    || out_vec == nullptr) {
        std::cerr <<
        "[sparse_gemv]: s < 1 or pointers are nullptrs." << std::endl;
        return;
    }
    int row_startidx = -1;
    int row_endidx = -1;
    T sum = 0.;
    for (unsigned int i = 0; i < s; ++i) {
        row_startidx = rowptr[sampled_rowidxs[i]];
        row_endidx = rowptr[sampled_rowidxs[i]+1];
        for (int j = row_startidx; j < row_endidx; ++j) {
            sum += data[j]*in_vec[colptr[j]];
        }
        out_vec[i] = sum*alpha;
        sum = 0.;
    }
}

template <typename T>
void sparse_gemv_transposed(unsigned int s, int* sampled_rowidxs, T alpha,
int* rowptr, int* colptr, T* data, T* in_vec, T* out_vec) {
    if (s < 1 || sampled_rowidxs == nullptr || rowptr == nullptr
    || colptr == nullptr || data == nullptr || in_vec == nullptr
    || out_vec == nullptr) {
        std::cerr <<
        "[sparse_gemv_transposed]: s < 1 or pointers are nullptrs."
        << std::endl;
        return;
    }
    int row_startidx = -1;
    int row_endidx = -1;
    for (unsigned int i = 0; i < s; ++i) {
        row_startidx = rowptr[sampled_rowidxs[i]];
        row_endidx = rowptr[sampled_rowidxs[i]+1];
        for (int j = row_startidx; j < row_endidx; ++j) {
            out_vec[colptr[j]] += alpha*data[j]*in_vec[i];
        }
    }
}

template <typename T>
void dense_gemv(unsigned int nrows, T alpha, T* labels_i, T* labels_j,
T* data, T* in_vec, T* out_vec) {
    if (nrows <= 0 || data == nullptr || labels_i == nullptr
    || labels_j == nullptr || in_vec == nullptr || out_vec == nullptr) {
        std::cerr <<
        "[dense_gemv]: nrows <= 0 or pointers are nullptrs." << std::endl;
        return;
    }
    T sum = 0.;
    for (unsigned int i = 0; i < nrows; ++i) {
        for (unsigned int j = 0; j < nrows; ++j) {
            sum += labels_j[j]*labels_i[i]*data[nrows*j + i]*in_vec[j];
        }
        out_vec[i] += alpha*sum;
        sum = 0.;
    }
}

template <typename T>
void rowmaj_dense_gemv(unsigned int nrows, T alpha, T* labels_i, T* labels_j,
T* data, T* in_vec, T* out_vec) {
    if (nrows <= 0 || data == nullptr || labels_i == nullptr
    || labels_j == nullptr || in_vec == nullptr || out_vec == nullptr) {
        std::cerr <<
        "[dense_gemv]: nrows <= 0 or pointers are nullptrs." << std::endl;
        return;
    }
    T sum = 0.;
    for (unsigned int i = 0; i < nrows; ++i) {
        for (unsigned int j = 0; j < nrows; ++j) {
            sum += labels_j[j]*labels_i[i]*data[nrows*i + j]*in_vec[j];
        }
        out_vec[i] += alpha*sum;
        sum = 0.;
    }
}

template <typename T>
void blocked_compute_gram_matrix(unsigned int s, unsigned int batch_size,
int* gramidxs, T alpha, int* rowptr, int* colptr, T* data, T*dense_out_mat) {
    if (s < 0 || batch_size < 1 || gramidxs == nullptr    || rowptr == nullptr
    || colptr == nullptr || data == nullptr || dense_out_mat == nullptr) {
        std::cerr <<
            "[blocked_compute_gram_matrix]: "
            "s < 0, batch_size < 1 or pointers are nullptrs"
            << std::endl;
    }
    // no pairwise gemms possible since s = 1. So just return without error.
    if (s == 1) {
        return;
    }

    int row_startidx = -1;
    int row_endidx = -1;
    int col_startidx = -1;
    int col_endidx = -1;
    unsigned int batch_row_offset = 0;
    unsigned int batch_col_offset = 0;
    unsigned int write_location = 0;
    const unsigned int batch_nvals = batch_size*batch_size;
    T sum = 0.;

    // loop over the s batches of gramidxs
    for (unsigned int i = 0; i < s; ++i) {
        batch_row_offset = i*batch_size;
        // just need unique pairwise gemms, hence j = [i+1...s-1].
        // all other pairwise gemms are redundant
        // because Gram matrix is symmetric.
        for (unsigned int j = i+1; j < s; ++j) {
            batch_col_offset = j*batch_size;
            // for each row in left-hand matrix/batch.
            for (unsigned int k = batch_row_offset;
                k < batch_row_offset+batch_size; ++k) {
                row_startidx = rowptr[gramidxs[k]];
                row_endidx = rowptr[gramidxs[k]+1];
                // for each column in right-hand matrix/batch.
                for (unsigned int l = batch_col_offset;
                l < batch_col_offset+batch_size; ++l) {
                    col_startidx = rowptr[gramidxs[l]];
                    col_endidx = rowptr[gramidxs[l]+1];
                    // for each element in row.
                    for (int m = row_startidx; m < row_endidx; ++m) {
                        // for each element in column.
                        for (int n = col_startidx; n < col_endidx; ++n) {
                            // colptr is assumed sorted.
                            // break from loop if not equal.
                            if (colptr[m] == colptr[n]) {
                                sum += data[m]*data[n];
                                col_startidx = n;
                                break;
                            }
                        }
                    }
                    /*    formula (((j*(j-1))/2 + i) computes where the (i,j) block gemm output is written.
                            corresponds to linearized lower triangular part of Gram matrix.
                            multiplying by batch_nvals provides the block offset.

                            Note: This is the location of the block.
                    */
                    /*
                            we assume dense_mat_out will have row-major ordering.
                            corresponds to rowidx*batch_size + colidx.

                            k - batch_row_offset ensures that k goes from 0...batch_size -1.
                            l - batch_col_offset ensures that l goes from 0...batch_size - 1.
                            Note: This is the location of the element within a block.
                    */
                    write_location = ((j*(j-1))/2 + i)*batch_nvals;
                    write_location += ((k-batch_row_offset)*batch_size)
                    + (l - batch_col_offset);
                    dense_out_mat[write_location] = alpha*sum;
                    sum = 0.;
                }
            }
        }
    }
    return;
}

template <typename T>
void sampled_csr_gemm(unsigned int len_rowidxs, int* rowidxs,
unsigned int len_colidxs, int* colidxs, T alpha, int* rowptr,
int* colptr, T* data, T* dense_out_mat) {
    if (rowidxs == nullptr || colidxs == nullptr || rowptr == nullptr
    || colptr == nullptr || data == nullptr || dense_out_mat == nullptr) {
        std::cerr <<
            "[sampled_csr_gemm]: pointers are nullptrs"
            << std::endl;
    }
    int row_startidx = -1;
    int row_endidx = -1;
    int col_startidx = -1;
    int col_endidx = -1;
    unsigned int offset = 0;
    T sum = 0.;
    for (unsigned int i = 0; i < len_rowidxs; ++i) {
        offset = i*len_rowidxs;
        row_startidx = rowptr[rowidxs[i]];
        row_endidx = rowptr[rowidxs[i]+1];
        for (unsigned int j = 0; j < len_colidxs; ++j) {
            col_startidx = rowptr[colidxs[j]];
            col_endidx = rowptr[colidxs[j]+1];
            for (int k = row_startidx; k < row_endidx; ++k) {
                for (int l = col_startidx; l < col_endidx; ++l) {
                    if (colptr[k] < colptr[l]) {
                        break;
                    } else if (colptr[k] == colptr[l]) {
                        sum += data[k] * data[l];
                        col_startidx = l;
                        break;
                    }
                }
            }
            dense_out_mat[offset + j] = alpha*sum;
            sum = 0.;
        }
    }
    return;
}

template <typename T>
void batched_compute_gram_matrix(unsigned int s, unsigned int batch_size,
int* gramidxs, T alpha, int* rowptr, int* colptr, T* data, T* dense_out_mat) {
    if (s < 1 || batch_size < 1 || gramidxs == nullptr || rowptr == nullptr
    || colptr == nullptr || data == nullptr || dense_out_mat == nullptr) {
        std::cerr <<
        "[batched_compute_gram_matrix]: " <<
        "s < 1 or batch_size < 1 or pointers are nullptrs." << std::endl;
        return;
    }
    int row_startidx = -1;
    int row_endidx = -1;
    int col_startidx = -1;
    int col_endidx = -1;
    unsigned int offset = 0;
    T sum = 0.;
    for (unsigned int i = 0; i < s*batch_size; ++i) {
        row_startidx = rowptr[gramidxs[i]];
        row_endidx = rowptr[gramidxs[i]+1];
        for (unsigned int j = i + (batch_size - (i%batch_size));
            j < s*batch_size; ++j) {
            col_startidx = rowptr[gramidxs[j]];
            col_endidx = rowptr[gramidxs[j]+1];
            for (int k = row_startidx; k < row_endidx; ++k) {
                // assumes sorted column indexes.
                for (int l = col_startidx; l < col_endidx; ++l) {
                    if (colptr[k] < colptr[l]) {
                        break;
                    } else if (colptr[k] == colptr[l]) {
                        sum += data[k]*data[l];
                        // advance col_startidx to reduce idx search since
                        // colptr is sorted.
                        col_startidx = l;
                        break;
                    }
                }
            }
            dense_out_mat[offset] = alpha*sum;
            offset++;
            sum = 0.;
        }
    }
    return;
}

template <typename T>
void compute_gram_matrix(unsigned int s, int* gramidxs, T alpha,
int* rowptr, int* colptr, T* data,    T* dense_out_mat) {
    if (s < 1 || gramidxs == nullptr || rowptr == nullptr || colptr == nullptr
    || data == nullptr || dense_out_mat == nullptr) {
        std::cerr <<
        "[compute_gram_matrix]: s < 1 or pointers are nullptrs." << std::endl;
        return;
    }
    int row_startidx = -1;
    int row_endidx = -1;
    int col_startidx = -1;
    int col_endidx = -1;
    unsigned int offset = 0;
    T sum = 0.;
    for (unsigned int i = 0; i < s; ++i) {
        row_startidx = rowptr[gramidxs[i]];
        row_endidx = rowptr[gramidxs[i]+1];
        for (unsigned int j = i+1; j < s; ++j) {
            offset = (j*(j-1))/2;
            col_startidx = rowptr[gramidxs[j]];
            col_endidx = rowptr[gramidxs[j]+1];
            for (int k = row_startidx; k < row_endidx; ++k) {
                // assumes sorted column indexes.
                for (int l = col_startidx; l < col_endidx; ++l) {
                    if (colptr[k] < colptr[l]) {
                        break;
                    } else if (colptr[k] == colptr[l]) {
                        sum += data[k]*data[l];
                        // advance col_startidx to reduce idx search since
                        // colptr is sorted.
                        col_startidx = l;
                        break;
                    }
                }
            }
            dense_out_mat[offset + i] = alpha*sum;
            sum = 0.;
        }
    }
}


// template instantiations.
// ToDo(Aditya): reduced-precision instantiations (when supported).
template float training_loss<float>
(MPI_Comm, unsigned int, int*, int*, float*, float*, float*);
template double training_loss<double>
(MPI_Comm, unsigned int, int*, int*, double*, double*, double*);
template float training_accuracy<float>
(MPI_Comm, unsigned int, int*, int*, float*, float*, float*);
template double training_accuracy<double>
(MPI_Comm, unsigned int, int*, int*, double*, double*, double*);
template void logistic_regression<float>
(MPI_Comm, Options<float>, int*, int*, float*, float*, float*);
template void logistic_regression<double>
(MPI_Comm, Options<double>, int*, int*, double*, double*, double*);
template void mini_batch_logistic_regression<float>
(MPI_Comm, Options<float>, int*, int*, float*, float*, float*);
template void mini_batch_logistic_regression<double>
(MPI_Comm, Options<double>, int*, int*, double*, double*, double*);
template void ca_logistic_regression<float>
(MPI_Comm, Options<float>, int*, int*, float*, float*, float*);
template void ca_logistic_regression<double>
(MPI_Comm, Options<double>, int*, int*, double*, double*, double*);
template void mini_batch_ca_logistic_regression<float>
(MPI_Comm, Options<float>, int*, int*, float*, float*, float*);
template void mini_batch_ca_logistic_regression<double>
(MPI_Comm, Options<double>, int*, int*, double*, double*, double*);
template void sparse_axpy<float>
(int, float, int*, int*, float*, float*);
template void sparse_axpy<double>
(int, double, int*, int*, double*, double*);
template void sparse_scal<float>
(int, float, int*, int*, float*, float*);
template void sparse_scal<double>
(int, double, int*, int*, double*, double*);
template void sparse_dot<float>
(int, int*, int*, float*, float*, float*);
template void sparse_dot<double>
(int, int*, int*, double*, double*, double*);
template void sparse_gemv<float>
(unsigned int, int*, float, int*, int*, float*, float*, float*);
template void sparse_gemv<double>
(unsigned int, int*, double, int*, int*, double*, double*, double*);
template void sparse_gemv_transposed<float>
(unsigned int, int*, float, int*, int*, float*, float*, float*);
template void sparse_gemv_transposed<double>
(unsigned int, int*, double, int*, int*, double*, double*, double*);
template void axpy<float>(unsigned int, float, float*, float*);
template void axpy<double>(unsigned int, double, double*, double*);
template void scal<float>(unsigned int, float, float*);
template void scal<double>(unsigned int, double, double*);
template void dot<float>(unsigned int, float*, float*, float*);
template void dot<double>(unsigned int, double*, double*, double*);
template void dot_unrolled<float>(unsigned int, float*, float*, float*);
template void dot_unrolled<double>(unsigned int, double*, double*, double*);
template void dense_gemv<float>
(unsigned int, float, float*, float*, float*, float*, float*);
template void dense_gemv<double>
(unsigned int, double, double*, double*, double*, double*, double*);
template void rowmaj_dense_gemv<float>
(unsigned int, float, float*, float*, float*, float*, float*);
template void rowmaj_dense_gemv<double>
(unsigned int, double, double*, double*, double*, double*, double*);
template void gemv<float>
(unsigned int, float, int*, int*, float*, float*, float*);
template void gemv<double>
(unsigned int, double, int*, int*, double*, double*, double*);
template void compute_gram_matrix<float>
(unsigned int, int*, float, int*, int*, float*, float*);
template void compute_gram_matrix<double>
(unsigned int, int*, double, int*, int*, double*, double*);
template void blocked_compute_gram_matrix<float>(unsigned int, unsigned int,
int*, float, int*, int*, float*, float*);
template void blocked_compute_gram_matrix<double>(unsigned int, unsigned int,
int*, double, int*, int*, double*, double*);
template void batched_compute_gram_matrix<float>(unsigned int, unsigned int,
int*, float, int*, int*, float*, float*);
template void batched_compute_gram_matrix<double>(unsigned int, unsigned int,
int*, double, int*, int*, double*, double*);
template void sampled_csr_gemm<float>
(unsigned int, int*, unsigned int, int*, float, int*, int*, float*, float*);
template void sampled_csr_gemm<double>
(unsigned int, int*, unsigned int, int*, double, int*, int*, double*, double*);
