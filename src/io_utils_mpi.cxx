// Copyright 2019 Aditya Devarakonda. All rights reserved.
#include "io_utils_mpi.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <cstring>
#include <cstdlib>

#include "utils.hpp"

/*
  Read LIBSVM-format binary classification data.
  This method might perform ok for small files. However, for large files a chunked approach is better.
*/

template <typename T>
Read_Statistics parallel_read_static_lb(MPI_Comm comm, const char* filename,
int** out_rowptr, int** out_colptr, T** out_data, T** out_labels) {
  Read_Statistics rd_stats;
  rd_stats.nrows = rd_stats.ncols = rd_stats.nnz = 0;
  rd_stats.nrows_total = rd_stats.ncols_total = rd_stats.nnz_total = 0;
  if (filename == nullptr) {
    std::cerr << "[parallel_read]: filename is nullptr." << std::endl;
    return rd_stats;
  }
  int mpi_rank;
  int mpi_size;
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);
  // TODO(Aditya): test whether manually resized raw-arrays
  // are faster than std::vec -> raw-arrays conversion.
  std::vector<int> rowptr, colptr;
  std::vector<T> data, labels;
  std::ifstream datafile(filename);
  std::string line;
  std::string buf = "";
  unsigned int idx = 0;
  int nnz_row = 0;
  double val = 0.;
  unsigned int ncol_max  = 0;
  bool one_two_labels = false;
  // read file line by line. store a copy of labels on all ranks.
  // Find ncol_max needed for column splitting.
  while (std::getline(datafile, line)) {
      std::istringstream str_stream(line);
      buf = "";
      rd_stats.nrows_total++;
      while (str_stream >> buf) {
          size_t colon_pos = buf.find(":");
          if (colon_pos == std::string::npos) {  // this is a label.
              if (buf.find('+') != std::string::npos) {
                  labels.push_back(1.);
              } else if (buf.find('-') != std::string::npos) {
                  labels.push_back(-1.);
              } else if (buf.find('2') != std::string::npos) {
                  if (!one_two_labels) {
                  // correct all labels '1' to '-1',
                  // because dataset has a label called '2'.
                    for (unsigned int i = 0; i < labels.size(); ++i) {
                      labels[i] = (labels[i] == static_cast<T>(1.))
                      ? static_cast<T>(-1.) : labels[i];
                    }
                    one_two_labels = true;
                  }
                  labels.push_back(1.);
              } else if (buf.find('1') != std::string::npos) {
                  if (one_two_labels) {
                    labels.push_back(-1.);
                  } else {
                    labels.push_back(1.);
                  }
              } else if (buf.find('0') != std::string::npos) {
                    labels.push_back(-1.);
              } else {
                  std::cout << buf << std::endl;
                  std::cerr << "Unhandled character found." << std::endl;
                  return rd_stats;
              }
          } else {
             idx = std::stoi(buf.substr(0, colon_pos));
             rd_stats.nnz_total++;
             // val = std::stod(buf.substr(colon_pos+1, buf.length()));
              ncol_max = (idx > ncol_max) ? idx : ncol_max;
              // nnz_row++;
              // colptr.push_back(idx-1);
              // data.push_back(val);
          }
      }
      // std::cout << "nnz this row: " << nnz_row << std::endl;
      // rowptr.push_back(nnz_row);
  }
  rd_stats.ncols_total = ncol_max;
  // std::cout << "ncols_total = " << ncol_max << std::endl;
  rd_stats.ncols = ncol_max/mpi_size;
  int col_remainder = ncol_max % mpi_size;
  unsigned int col_offset = mpi_rank*rd_stats.ncols;
  if (mpi_rank < col_remainder) {
    rd_stats.ncols++;
    col_offset += mpi_rank;
  } else {
    col_offset += col_remainder;
  }
  unsigned int col_idx_end = col_offset+rd_stats.ncols;
  rowptr.clear(), colptr.clear(), data.clear();
  rowptr.push_back(0);
  nnz_row = 0;
  datafile.clear();
  datafile.seekg(0, std::ios::beg);
  while (std::getline(datafile, line)) {
      std::istringstream str_stream(line);
      buf = "";
      while (str_stream >> buf) {
          size_t colon_pos = buf.find(":");
          if (colon_pos != std::string::npos) {  // this is not a label.
            // cast and convert to 0-indexing.
            idx = std::stoi(buf.substr(0, colon_pos)) - 1;
            val = std::stod(buf.substr(colon_pos+1, buf.length()));
            if (col_offset <= idx && idx < col_idx_end) {
              nnz_row++;
              colptr.push_back(idx-col_offset);
              data.push_back(static_cast<T>(val));
            }
          }
      }
      rowptr.push_back(nnz_row);
  }
  // broadcast assigned_ncols so all ranks can perform read.
    // compute offsets
  rd_stats.nnz = nnz_row;
  rd_stats.nrows = static_cast<unsigned int>(labels.size());
  // ALIGNED_ALLOC, memcpy, and std::vector<>.clear()
  // are called in sequence to reduce mem. overhead.
  *out_rowptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*rowptr.size()));
  if (*out_rowptr == nullptr) {
      std::cerr << "[parallel_read]: out_rowptr is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_rowptr, rowptr.data(), sizeof(int)*rowptr.size());
  rowptr.clear();
  *out_colptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*colptr.size()));
  if (*out_colptr == nullptr) {
      std::cerr << "[parallel_read]: out_colptr is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_colptr, colptr.data(), sizeof(int)*colptr.size());
  colptr.clear();
  *out_data = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*data.size()));
  if (*out_data == nullptr) {
      std::cerr << "[parallel_read]: out_data is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_data, data.data(), sizeof(T)*data.size());
  data.clear();
  *out_labels = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*labels.size()));
  if (*out_labels == nullptr) {
      std::cerr << "[parallel_read]: out_labels is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_labels, labels.data(), sizeof(T)*labels.size());
  labels.clear();
  return rd_stats;
}

template <typename T>
Read_Statistics parallel_read(MPI_Comm comm, const char* filename,
int** out_rowptr, int** out_colptr, T** out_data, T** out_labels) {
  Read_Statistics rd_stats;
  rd_stats.nrows = rd_stats.ncols = rd_stats.nnz = 0;
  rd_stats.nrows_total = rd_stats.ncols_total = rd_stats.nnz_total = 0;
  if (filename == nullptr) {
    std::cerr << "[parallel_read]: filename is nullptr." << std::endl;
    return rd_stats;
  }
  int mpi_rank;
  int mpi_size;
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);
  // TODO(Aditya): test whether manually resized raw-arrays
  // are faster than std::vec -> raw-arrays conversion.
  std::vector<int> rowptr, colptr;
  std::vector<T> data, labels;
  std::ifstream datafile(filename);
  std::string line;
  std::string buf = "";
  unsigned int idx = 0;
  int nnz_row = 0;
  double val = 0.;
  unsigned int ncol_max  = 0;
  bool one_two_labels = false;
  // read file line by line. store a copy of labels on all ranks.
  // Find ncol_max needed for column splitting.
  while (std::getline(datafile, line)) {
      std::istringstream str_stream(line);
      buf = "";
      rd_stats.nrows_total++;
      while (str_stream >> buf) {
          size_t colon_pos = buf.find(":");
          if (colon_pos == std::string::npos) {  // this is a label.
              if (buf.find('+') != std::string::npos) {
                  labels.push_back(1.);
              } else if (buf.find('-') != std::string::npos) {
                  labels.push_back(-1.);
              } else if (buf.find('2') != std::string::npos) {
                  if (!one_two_labels) {
                  // correct all labels '1' to '-1',
                  // because dataset has a label called '2'.
                    for (unsigned int i = 0; i < labels.size(); ++i) {
                      labels[i] = (labels[i] == static_cast<T>(1.))
                      ? static_cast<T>(-1.) : labels[i];
                    }
                    one_two_labels = true;
                  }
                  labels.push_back(1.);
              } else if (buf.find('1') != std::string::npos) {
                  if (one_two_labels) {
                    labels.push_back(-1.);
                  } else {
                    labels.push_back(1.);
                  }
              } else if (buf.find('0') != std::string::npos) {
                    labels.push_back(-1.);
              } else {
                  std::cout << buf << std::endl;
                  std::cerr << "Unhandled character found." << std::endl;
                  return rd_stats;
              }
          } else {
             idx = std::stoi(buf.substr(0, colon_pos));
             rd_stats.nnz_total++;
              ncol_max = (idx > ncol_max) ? idx : ncol_max;
          }
      }
  }
  rd_stats.ncols_total = ncol_max;
  rd_stats.ncols = ncol_max/mpi_size;
  int col_remainder = ncol_max % mpi_size;
  unsigned int col_offset = mpi_rank*rd_stats.ncols;
  int num_cols = rd_stats.ncols+1;
  int* nnz_per_col = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*(num_cols)));
  std::cout << "Here" << std::endl;
  int* assigned_ncols = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*mpi_size));
  if (nnz_per_col == nullptr || assigned_ncols == nullptr) {
    std::cerr << "nnz_per_col/assigned_ncols are nullptrs." << std::endl;
    return rd_stats;
  }
  std::memset(nnz_per_col, 0, sizeof(int)*(num_cols));
  std::memset(assigned_ncols, 0, sizeof(int)*mpi_size);
  int* nnz_per_col_all_ranks = 0;
  if (mpi_rank == 0) {
    nnz_per_col_all_ranks = static_cast<int*>
    (ALIGNED_ALLOC(MEM_ALIGN, mpi_size*sizeof(int)*num_cols));
  }
  if (mpi_rank < col_remainder) {
    rd_stats.ncols++;
    col_offset += mpi_rank;
  } else {
    col_offset += col_remainder;
  }
  unsigned int col_idx_end = col_offset+rd_stats.ncols;
  bool load_data = false;
  bool exit_loop = false;
  int avg_nnz_per_rank = rd_stats.nnz_total/mpi_size;
  rowptr.clear(), colptr.clear(), data.clear();
  rowptr.push_back(0);
  while (!exit_loop) {
    nnz_row = 0;
    datafile.clear();
    datafile.seekg(0, std::ios::beg);
    while (std::getline(datafile, line)) {
        std::istringstream str_stream(line);
        buf = "";
        while (str_stream >> buf) {
            size_t colon_pos = buf.find(":");
            if (colon_pos != std::string::npos) {  // this is not a label.
              // cast and convert to 0-indexing.
              idx = std::stoi(buf.substr(0, colon_pos)) - 1;
              val = std::stod(buf.substr(colon_pos+1, buf.length()));
              if (col_offset <= idx && idx < col_idx_end) {
                nnz_row++;
                if (load_data) {
                  colptr.push_back(idx-col_offset);
                  data.push_back(static_cast<T>(val));
                } else {
                  nnz_per_col[idx-col_offset]++;
                }
              }
            }
        }
        if (load_data) {
         rowptr.push_back(nnz_row);
        }
    }
    if (load_data == true) {
      rd_stats.nnz = nnz_row;
      exit_loop = true;
      break;
    }
    MPI_Gather(nnz_per_col, num_cols, MPI_INT,
    nnz_per_col_all_ranks, num_cols, MPI_INT, 0, comm);
    if (mpi_rank == 0) {
      int nnz_count = 0;
      int ncols = 0;
      int total_ncols = 0;
      int assigned_rank = 0;
      int extra_col = 0;
      for (int i = 0; i < num_cols*mpi_size; ++i) {
        ncols += (nnz_per_col_all_ranks[i] == 0) ? 0 : 1;
        nnz_count += nnz_per_col_all_ranks[i];
        if (nnz_count > avg_nnz_per_rank) {
          ncols = (ncols == 1) ? ((rd_stats.ncols_total-total_ncols)
                /(mpi_size-assigned_rank)) : (ncols - 1);
          i -= (i == 0) ? 0 : 1;
          assigned_ncols[assigned_rank] = ncols;
          total_ncols += ncols;
          ++assigned_rank;
          nnz_count = 0;
          ncols = 0;
        }
        if (assigned_rank == mpi_size) {break;}
      }
      extra_col = rd_stats.ncols_total - total_ncols;
      assigned_ncols[mpi_size - 1] += extra_col;
    }
    // broadcast assigned_ncols so all ranks can perform read.
    MPI_Bcast(assigned_ncols, mpi_size, MPI_INT, 0, comm);
    col_offset = 0;
    for (int i = 0; i < mpi_rank; ++i) {
      col_offset += assigned_ncols[i];
    }
    rd_stats.ncols = assigned_ncols[mpi_rank];
    col_idx_end = col_offset + rd_stats.ncols;
    load_data = true;
    // compute offsets
  }
  rd_stats.nnz = nnz_row;
  rd_stats.nrows = static_cast<unsigned int>(labels.size());
  // ALIGNED_ALLOC, memcpy, and std::vector<>.clear()
  // are called in sequence to reduce mem. overhead.
  *out_rowptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*rowptr.size()));
  if (*out_rowptr == nullptr) {
      std::cerr << "[parallel_read]: out_rowptr is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_rowptr, rowptr.data(), sizeof(int)*rowptr.size());
  rowptr.clear();
  *out_colptr = static_cast<int*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(int)*colptr.size()));
  if (*out_colptr == nullptr) {
      std::cerr << "[parallel_read]: out_colptr is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_colptr, colptr.data(), sizeof(int)*colptr.size());
  colptr.clear();
  *out_data = static_cast<T*>(ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*data.size()));
  if (*out_data == nullptr) {
      std::cerr << "[parallel_read]: out_data is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_data, data.data(), sizeof(T)*data.size());
  data.clear();
  *out_labels = static_cast<T*>
  (ALIGNED_ALLOC(MEM_ALIGN, sizeof(T)*labels.size()));
  if (*out_labels == nullptr) {
      std::cerr << "[parallel_read]: out_labels is a nullptr." << std::endl;
      return rd_stats;
  }
  std::memcpy(*out_labels, labels.data(), sizeof(T)*labels.size());
  labels.clear();
  return rd_stats;
}

/*
  A chunked reader. This should be faster than read() for large files.
*/
int parallel_chunked_read(MPI_Comm comm, const char* filename,
std::vector<int>* rowptr, std::vector<int>* colptr, std::vector<double>* data,
std::vector<double>* labels) {
  // TODO(Aditya): Finish implementation.
  return 0;
}

/*
  A multi-threaded reader. This should be faster than read()
  since multiple threads are involved.
*/
int parallel_threaded_read(MPI_Comm comm, const char* filename,
std::vector<int>* rowptr, std::vector<int>* colptr, std::vector<double>* data,
std::vector<double>* labels) {
  // TODO(Aditya): Finish implementation.
  return 0;
}

/*
  A multi-threaded and chunked reader. This should be faster than read()
  since multiple threads are involved.
*/
int parallel_threaded_chunked_read(MPI_Comm comm, const char* filename, std::vector<int>* rowptr,
std::vector<int>* colptr, std::vector<double>* data,
std::vector<double>* labels) {
  // TODO(Aditya): Finish implementation.
  return 0;
}

template <typename T>
void parallel_write(MPI_Comm comm, const char* filename,
Read_Statistics rd_stats, int* rowptr, int* colptr, T* data, T* labels) {
  if (filename == nullptr || rowptr == nullptr || colptr == nullptr
  || data == nullptr || labels == nullptr) {
    std::cerr << "[parallel_write]: pointers are nullptrs." << std::endl;
    return;
  }
  if (rd_stats.nrows <= 0 || rd_stats.ncols <= 0 || rd_stats.nnz <= 0) {
    std::cerr << "[parallel_write]: rd_stats settings are invalid."
    << std::endl;
    return;
  }
  int mpi_rank, mpi_size;
  MPI_Comm_rank(comm, &mpi_rank);
  MPI_Comm_size(comm, &mpi_size);
  std::ofstream file(filename, std::ofstream::out);
  std::string buf = "";
  for (unsigned int i = 1; i < rd_stats.nrows+1; ++i) {
      if (labels[i-1] == -1. && mpi_rank == 0) {
          buf += "1 ";
      } else if (labels[i-1] == 1. && mpi_rank == 0) {
          buf += "2 ";
      }
      for (int j = rowptr[i-1]; j < rowptr[i]; ++j) {
          buf += std::to_string(colptr[j]+1) + ":"
          + std::to_string(static_cast<int>(data[j])) + " ";
      }
      buf += "\n";
  }
  file << buf;
  file.close();
}

template Read_Statistics parallel_read_static_lb<double>
(MPI_Comm, const char*, int**, int**, double**, double**);
template Read_Statistics parallel_read_static_lb<float>
(MPI_Comm, const char*, int**, int**, float**, float**);
template Read_Statistics parallel_read<double>
(MPI_Comm, const char*, int**, int**, double**, double**);
template Read_Statistics parallel_read<float>
(MPI_Comm, const char*, int**, int**, float**, float**);
template void parallel_write<double>
(MPI_Comm, const char*, Read_Statistics, int*, int*, double*, double*);
template void parallel_write<float>
(MPI_Comm, const char*, Read_Statistics, int*, int*, float*, float*);
